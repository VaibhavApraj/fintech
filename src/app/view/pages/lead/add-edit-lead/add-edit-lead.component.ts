import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadService } from '../../../../core/lead/service/lead.service';
import { LeadElement } from '../../../../core/lead/models/lead.model'
import { StatesElement } from 'src/app/core/geography-masters/states/models/states.model';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';
import { PincodeElement } from 'src/app/core/geography-masters/pincode/models/pincode.model';
import { PincodeService } from 'src/app/core/geography-masters/pincode/service/pincode.service';
import { LeadSourceElement } from 'src/app/core/lead-source/models/leadSource.model';
import { LeadSourceService } from 'src/app/core/lead-source/service/leadSource.service';
import { UserElement } from 'src/app/core/user/models/user.model';
import { AuthService } from 'src/app/core/auth/service/auth.service';
import * as moment from 'moment';
import { LeadStatusElement } from 'src/app/core/lead-status/models/leadStatus.model';
import { LeadStatusService } from 'src/app/core/lead-status/service/leadStatus.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-edit-lead',
  templateUrl: './add-edit-lead.component.html',
  styleUrls: ['./add-edit-lead.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditLeadComponent implements OnInit {
  addEditForm: FormGroup
  genderName = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'}
  ];
  // countryName = [
  //   {value: '1', viewValue: 'India'},
  //   {value: '2', viewValue: 'USA'},
  //   {value: '3', viewValue: 'Itali'},
  //   {value: '4', viewValue: 'Africa'}
  // ];
  // stateName = [
  //   {value: '1', viewValue: 'Maharastra'},
  //   {value: '2', viewValue: 'Delhi'},
  //   {value: '3', viewValue: 'Chennai'},
  //   {value: '4', viewValue: 'Gujarat'}
  // ];
  // cityName = [
  //   {value: '1', viewValue: 'Mumabi'},
  //   {value: '2', viewValue: 'Pune'},
  //   {value: '3', viewValue: 'Nashik'},
  //   {value: '4', viewValue: 'Nagpur'}
  // ];
  salutationArray= [
    {value: '1', viewValue: 'Mr.'},
    {value: '2', viewValue: 'Mrs.'},
    {value: '3', viewValue: 'Ms.'},
    {value: '4', viewValue: 'Miss'},
    {value: '5', viewValue: 'Sir'}
  ];
  countryName:CountriesElement[]=[];
  StateName:StatesElement[]=[];
  CityName:CitiesElement[]=[];
  PincodeName:PincodeElement[]=[];
  LeadSourceName:LeadSourceElement[]=[];
  LeadStatusName: LeadStatusElement[]=[];
  AssignToName:any=[];

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';
  _addEditFormData:any;
   countryPlaceHolder='Select Country'
   cityPlaceHolder='Select City'
   satetPlaceHolder='Select State'
   pincodePlaceHolder='Select Pincode'
   leadSourcePlaceHolder='Select Lead Source'
   AsssinedToPlaceHolder='Select Asssined To'
   leadStatusPlaceholderName:any='Open';
   forValue:any=''

   isValidAge = false;
  userDetails:any;
  userDetailAtoBValue:any='';
  roleArray:any=[];
  maxDob: Date;
  assignToHideForSales:boolean=true;
  allLeadDetails: any = '';
  viewLeadAllDetailsOnly: boolean = false;
  hideAddEditForm: boolean = true;
  hideOnlySealesSchecdule: boolean = true;
  salesRole: boolean = false;
  spliteRoleName: any;
  makeaRoleArray: any;
  leadCreatedByName:any='';
  hideAssignTo:boolean=false;
  hideRejectReason:boolean=false
  constructor(private toastr: ToastrService,private leadStatusService:LeadStatusService,private authService:AuthService,private pincodeService:PincodeService,private countriesService:CountriesService,private statesService:StatesService,private citiesService:CitiesService,private fb: FormBuilder,private router: Router, private leadService: LeadService,private routes:ActivatedRoute,private leadSourceService:LeadSourceService) {

    this.spliteRoleName = sessionStorage.getItem('role');
    this.makeaRoleArray = this.spliteRoleName?.split(',');


    for (let j = 0; j < this.makeaRoleArray.length; j++) {
      console.log(this.makeaRoleArray[j]);

      if (this.makeaRoleArray[j] == 'Sales') {
        //this.VerifyBtnDiv=true;
        this.salesRole = true
      }
    }

    const today = new Date();
  this.maxDob = new Date(
    today.getFullYear() - 18,//age more than 18
    today.getMonth(),
    today.getDate()
  );

    this.addEditForm = this.fb.group({
      id:[''],
      salutation: [''],
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      mobileNo: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      mobileNo2: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      dateOfBirth: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      pan: ['', Validators.compose([Validators.required,Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')])],
      aadhar: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      postalCode: ['',Validators.compose([Validators.required])],
      leadSource: ['', Validators.compose([Validators.required])],
      leadStage: [''],
      userId: [''],//, Validators.compose([Validators.required])
      area:[''],
      createdBy:[''],
      leadStatus:['']
    })

    //this.countryPlaceHolder='Select Country'
  }

  ngOnInit(): void {

      //debugger

       if (this.authService.isLoggedIn()) {
      this.userDetails=sessionStorage.getItem('UserDetails')
      console.log(this.userDetailAtoBValue=JSON.parse(atob(this.userDetails)));
      for(let i=0;i<this.userDetailAtoBValue.role.length;i++)
      {
        console.log(this.userDetailAtoBValue.role);

        this.roleArray.push(this.userDetailAtoBValue.role[i].roleName)
      }
      console.log(this.roleArray,this.roleArray);

      if(this.roleArray.includes('Sales'))
      {
        // this
        this.assignToHideForSales=false;
        // this.addEditForm.get('userId')?.clearValidators
        // this.addEditForm.get('userId')?.updateValueAndValidity;
        this.addEditForm.get('userId')?.patchValue(this.userDetailAtoBValue.id)
        this.addEditForm.get('createdBy')?.patchValue('Sales')
        this.leadCreatedByName='Sales';
      }
      // if(this.roleArray.includes('Call Center'))
      // {
      //   this.addEditForm.get('createdBy')?.patchValue('Call Cente')
      // }
      // if(this.roleArray.includes('Agency'))
      // {
      //   this.addEditForm.get('createdBy')?.patchValue('Agency')
      // }

      //dob validation 18

      this.addEditForm.get('dateOfBirth')?.valueChanges.subscribe((val: any) => {
        let current = moment();
        let selected = moment(val, 'MM-YYYY');
        let age = moment.duration(current.diff(selected));
        if (age.years() < 18) {
          this.isValidAge = true;
          this.addEditForm.get('dateOfBirth')?.setErrors({ incorrect: true });
        } else {
          this.isValidAge = false;
          this.addEditForm.get('dateOfBirth')?.setErrors(null);
        }
      });
    }

    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      this.getDropdownData()
      setTimeout(() => {
        this.getSingleData(this.queryParamData.id)
      }, 500);

    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();

      this.viewLeadAllDetailsOnly = true;
      this.hideAddEditForm = false;

      if(this.roleArray.includes('Sales'))
      {
        this.assignToHideForSales=true;
        this.hideOnlySealesSchecdule=false
        this.addEditForm.get('userId')?.patchValue(this.userDetailAtoBValue.id)
      }
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
      this.getDropdownData()
      // this.addEditHeadTitle='View'
      // this.saveBtn=false;
      // this.createBtn=false;
      // this.viewLeadAllDetailsOnly = true;
      // this.hideAddEditForm = false;

    }

    this.addEditForm.get('state')?.disable();
    this.addEditForm.get('country')?.disable();
    this.addEditForm.get('postalCode')?.disable();
    this.addEditForm.get('userId')?.disable();
    this.addEditForm.get('area')?.disable();
    this.addEditForm.get('leadStatus')?.disable();
  }

  getDropdownData(){
    // this.getCountryData();
    //this.getStateData();
    this.getCityData();
    this.getPincodeData();
    this.getLeadSourceData();
   // this.getLeadStatusData();
  }

  getLeadStatusData(){
    this.leadStatusService.getLeadStatusList().subscribe(res => {
      console.log(res, "getLeadStatusList Name");
      this.LeadStatusName = res.data;
    });
  }

  getCountryData(){
    this.countriesService.getCountriesList().subscribe(res => {
      console.log(res, "country Name");
      this.countryName=res.data;
    });
  }
  getStateData(){
    this.statesService.getStatesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.StateName=res;
    });
  }
  getCityData(){
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "citiesService Name");
      this.CityName=res.data;
    });
  }
  getPincodeData(){
    this.pincodeService.getPincodeList().subscribe(res => {
      console.log(res, "pincodeService Name");
      this.PincodeName=res;
    });
  }
  getLeadSourceData(){
    this.leadSourceService.getLeadSourceList().subscribe(res => {
      console.log(res, "LeadSourceName Name");
      this.LeadSourceName=res.data;
    });
  }
  getSingleData(id:any){
    this.leadService.getLeadById(id).subscribe(res => {
      console.log(res.data, "getLeadById");
      this.allLeadDetails=res.data;
      this.addEditForm.patchValue(res.data);

      // this.addEditForm.get('state')?.patchValue(res.data.state.stateId)
      // this.addEditForm.get('country')?.patchValue(res.data.country.countryId)

      if(res.data.user!=null){
        this.addEditForm.get('userId')?.patchValue(res.data.user.id)
        this.hideAssignTo=true;
        this.AsssinedToPlaceHolder=res.data.user.firstName +" "+res.data.user.lastName;
      }
      if(res.data.rejectReason != null){
        this.hideRejectReason=true;
      }


      // this.addEditForm.get('userId')?.patchValue(res.user
      //   )
    //  this.countryPlaceHolder='vaibhav'
    //  console.log(Number(res.country),this.countryName);

      // const counNameData= this.countryName.filter(resCountry=>{resCountry.id})
      //const forValue=''


      // for(let i=0;i<this.countryName.length;i++){
      //   if(this.countryName[i].id == res.country)
      //   {
      //       console.log(this.countryName[i].countryName);
      //       this.countryPlaceHolder=this.countryName[i].countryName
      //   }
      // }
      for(let i=0;i<this.CityName.length;i++){
        if(this.CityName[i].id == res.data.city)
        {
            console.log(this.CityName[i].cityName);
            this.cityPlaceHolder=this.CityName[i].cityName
        }
      }
      for(let i=0;i<this.StateName.length;i++){
        if(this.StateName[i].id == res.data.state)
        {
            console.log(this.StateName[i].stateName);
            this.satetPlaceHolder=this.StateName[i].stateName
        }
      }
      // for(let i=0;i<this.PincodeName.length;i++){
      //   if(this.PincodeName[i].id == res.postalCode)
      //   {
      //       console.log(this.PincodeName[i].pincode);
      //       this.pincodePlaceHolder=this.PincodeName[i].pincode
      //   }
      // }
      for(let i=0;i<this.LeadSourceName.length;i++){
        if(this.LeadSourceName[i].id == res.data.leadSource)
        {
            console.log(this.LeadSourceName[i].leadSourceName);
            this.leadSourcePlaceHolder=this.LeadSourceName[i].leadSourceName
        }
      }





      // this.placeHolderSetValue(this.countryName,this.countryPlaceHolder,res.country,'countryName');
      // this.placeHolderSetValue(this.CityName,this.cityPlaceHolder,res.city,'cityName');
      // this.placeHolderSetValue(this.StateName,this.satetPlaceHolder,res.state,'stateName');
      // this.placeHolderSetValue(this.PincodeName,this.pincodePlaceHolder,res.postalCode,'pincode');
      // this.placeHolderSetValue(this.LeadSourceName,this.leadSourcePlaceHolder,res.leadSource,'leadSourceName');

//      this.addEditForm.get('country')?.patchValue(res.country)

      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('salutation')?.patchValue(res.salutation)
      // this.addEditForm.get('firstName')?.patchValue(res.firstName)
      // this.addEditForm.get('city')?.patchValue(res.city)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)

      // id:[''],
      // salutation: ['', Validators.compose([Validators.required])],
      // firstName: ['', Validators.compose([Validators.required])],
      // lastName: ['', Validators.compose([Validators.required])],
      // mobileNo: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      // email: ['', Validators.compose([Validators.required, Validators.email])],
      // dateOfBirth: ['', Validators.compose([Validators.required])],
      // gender: ['', Validators.compose([Validators.required])],
      // pan: ['', Validators.compose([Validators.required])],
      // aadhar: ['', Validators.compose([Validators.required])],
      // city: ['', Validators.compose([Validators.required])],
      // country: ['', Validators.compose([Validators.required])],
      // state: ['', Validators.compose([Validators.required])],
      // postalCode: ['',Validators.compose([Validators.required])],
      // leadSource: ['', Validators.compose([Validators.required])],
      // leadStage: ['', Validators.compose([Validators.required])],
      // userId: ['', Validators.compose([Validators.required])],
    });
  }


  placeHolderSetValue(arrayname:any,placeholdername:any,id:any,objName:any){

    for(let i=0;i<arrayname.length;i++){
      if(arrayname[i].id == id)
      {
          console.log(arrayname[i].countryName);
          placeholdername=arrayname[i].objName
      }
    }
  }
  //selectyion change

  countrySelect(id:any)
  {

     console.log(id);
    //this.addEditForm.get('state')?.enable();
    this.countriesService.getStateListByCountryId(id).subscribe(res => {
      console.log(res, "getStateListByCountryId Name");
      this.StateName=res;


    });
    // this.cityPlaceHolder='Select City'
    // this.satetPlaceHolder='Select State'
    // this.pincodePlaceHolder='Select Pincode'
    // this.AsssinedToPlaceHolder='Select Asssined To'
  }
  stateSelect(id:any)
  {
    console.log(id);
      //this.addEditForm.get('city')?.enable();
      this.statesService.getCityListByStateId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.CityName=res;
      });
      // this.satetPlaceHolder='Select State'
      // this.pincodePlaceHolder='Select Pincode'
      // this.AsssinedToPlaceHolder='Select Asssined To'
  }
  citySelect(id:any)
  {
    console.log(id);
      this.addEditForm.get('postalCode')?.enable();
      this.citiesService.getPincodeListByCityId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.PincodeName=res;
      });
      this.citiesService.getAlldetailsByCityId(id).subscribe(res => {
        console.log(res, "getAlldetailsByCityId Name");
       // this.PincodeName=res;
       this.addEditForm.get('country')?.patchValue(res.data.countryId)
       this.addEditForm.get('state')?.patchValue(res.data.stateId)
       this.satetPlaceHolder=res.data.stateName
       this.countryPlaceHolder=res.data.countryName
      });

      // this.pincodePlaceHolder='Select Pincode'
      // this.AsssinedToPlaceHolder='Select Asssined To'
  }

  pincodeSelect(id:any)
  {
    console.log(id);
    //this.addEditForm.get('userId')?.enable();

      // this.pincodeService.getUserByPincodeId(id).subscribe(res => {
      //   console.log(res, "getCityListByStateId Name");
      //   this.AssignToName=res;
      // });

    //this.AsssinedToPlaceHolder='Select Asssined To'

    const areaValue =this.PincodeName.filter(res=>res.id==id)
    this.addEditForm.get('area')?.patchValue(areaValue[0].areaName)
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/lead', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    console.log(this.addEditForm);

    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this.addEditForm.get('leadStage')?.patchValue('New');
    this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.leadService.updateLeadById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('Lead Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/lead', { skipLocationChange: true });
      });
    }
    else{
      console.log(this.leadCreatedByName);

      if(this.addEditForm.get('userId')?.value == '')
      this._addEditFormData.userId=0;
      this._addEditFormData.country=this.addEditForm.get('country')?.value
      this._addEditFormData.state=this.addEditForm.get('state')?.value
      this._addEditFormData.area=this.addEditForm.get('area')?.value
      this._addEditFormData.createdBy=this.addEditForm.get('createdBy')?.value
      this._addEditFormData.userId=this.addEditForm.get('userId')?.value
      this._addEditFormData.leadStatus=1

      console.log(this._addEditFormData)

      this.leadService.createLead(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
        this.toastr.success('Lead Created Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/lead', { skipLocationChange: true });
       // this.autoAssignUserByRoleLead()
      });
    }


  }

  autoAssignUserByRoleLead(){
    this.leadService.autoAssignUserByRole(this.userDetailAtoBValue.id,this.roleArray).subscribe(res => {
      console.log(res, "res");
    });
  }

}
