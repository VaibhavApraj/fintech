import { AfterViewInit, Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { LeadService } from '../../../core/lead/service/lead.service';
import { LeadElement } from '../../../core/lead/models/lead.model'
import { delay } from 'rxjs';

// export interface UserData {
//   id: string;
//   name: string;
//   progress: string;
//   fruit: string;
// }

// const FRUITS: string[] = [
//   'blueberry',
//   'lychee',
//   'kiwi',
//   'mango',
//   'peach',
//   'lime',
//   'pomegranate',
//   'pineapple',
// ];
// const NAMES: string[] = [
//   'Maia',
//   'Asher',
//   'Olivia',
//   'Atticus',
//   'Amelia',
//   'Jack',
//   'Charlotte',
//   'Theodore',
//   'Isla',
//   'Oliver',
//   'Isabella',
//   'Jasper',
//   'Cora',
//   'Levi',
//   'Violet',
//   'Arthur',
//   'Mia',
//   'Thomas',
//   'Elizabeth',
// ];

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css'],
  // encapsulation:ViewEncapsulation.None
})
export class LeadComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: LeadElement[];
  displayedColumns: string[] = ['leadId','firstName', 'lastName', 'mobileNo', 'email','leadStage', 'action'];
  dataSource = new MatTableDataSource<LeadElement>(this.ELEMENT_DATA);

  callCenterRole:boolean=true;
  agencyRole:boolean=true;
  salesRole:boolean=true;
  creditRole:boolean=true;
  spliteRoleName:any;
  makeaRoleArray:any;

  constructor(private router: Router, private leadService: LeadService) {

    // const users = Array.from({ length: 100 }, (_, k) => createNewUser(k + 1));
    // this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {

    this.spliteRoleName=sessionStorage.getItem('role');
    this.makeaRoleArray= this.spliteRoleName?.split(',');


     for(let j=0;j<this.makeaRoleArray.length;j++)
    {
     console.log(this.makeaRoleArray[j]);
      if(this.makeaRoleArray[j]=='Call Center')
      {
        console.log(1);

        this.callCenterRole=false;
      }
      if(this.makeaRoleArray[j]=='Agency')
      {
        console.log(2);

        this.agencyRole=false;
      }
      if(this.makeaRoleArray[j]=='Sales')
      {
        console.log(3);

        this.salesRole=false;
      }
      if(this.makeaRoleArray[j]=='Credit')
      {
        console.log(4);

        this.creditRole=false;
      }
    }

    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/lead/add-edit-lead'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.leadService.getLeadList().subscribe(res => {
        //console.log(res, "res");
         //delay(1000)
        this.dataSource.data = res.data as LeadElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/lead/add-edit-lead', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

// function createNewUser(id: number): UserData {
//   const name =
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
//     ' ' +
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
//     '.';

//   return {
//     id: id.toString(),
//     name: name,
//     progress: Math.round(Math.random() * 100).toString(),
//     fruit: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
//   };
// }
