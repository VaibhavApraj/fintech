import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/auth/service/auth.service';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';
import { PincodeElement } from 'src/app/core/geography-masters/pincode/models/pincode.model';
import { PincodeService } from 'src/app/core/geography-masters/pincode/service/pincode.service';
import { StatesElement } from 'src/app/core/geography-masters/states/models/states.model';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';
import { LeadSourceElement } from 'src/app/core/lead-source/models/leadSource.model';
import { LeadSourceService } from 'src/app/core/lead-source/service/leadSource.service';
import { LeadService } from 'src/app/core/lead/service/lead.service';

@Component({
  selector: 'app-add-edit-enquiry',
  templateUrl: './add-edit-enquiry.component.html',
  styleUrls: ['./add-edit-enquiry.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddEditEnquiryComponent implements OnInit {
  addEditForm: FormGroup

  countryName:CountriesElement[]=[];
  StateName:StatesElement[]=[];
  CityName:CitiesElement[]=[];
  PincodeName:PincodeElement[]=[];
  LeadSourceName:LeadSourceElement[]=[];
  AssignToName:any=[];

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';
 // _addEditFormData:any;
   countryPlaceHolder='Select Country'
   cityPlaceHolder='Select City'
   satetPlaceHolder='Select State'
   pincodePlaceHolder='Select Pincode'
   leadSourcePlaceHolder='Select Lead Source'
   AsssinedToPlaceHolder='Select Asssined To'
   forValue:any=''


  userDetails:any;
  userDetailAtoBValue:any='';
  roleArray:any=[];
  assignToHideForSales:boolean=true;

  constructor(private toastr: ToastrService,private authService:AuthService,private pincodeService:PincodeService,private countriesService:CountriesService,private statesService:StatesService,private citiesService:CitiesService,private fb: FormBuilder,private router: Router, private leadService: LeadService,private routes:ActivatedRoute,private leadSourceService:LeadSourceService) {


    this.addEditForm = this.fb.group({
      id:[''],
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      mobileNo: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      product: ['',Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      postalCode: ['',Validators.compose([Validators.required])],
      leadStatus: [''],

    })
    this.getCityData();
    this.getPincodeData();

    //this.countryPlaceHolder='Select Country'
  }

  ngOnInit(): void {

       if (this.authService.isLoggedIn()) {
      this.userDetails=sessionStorage.getItem('UserDetails')
      console.log(this.userDetailAtoBValue=JSON.parse(atob(this.userDetails)));
      for(let i=0;i<this.userDetailAtoBValue.role.length;i++)
      {
        console.log(this.userDetailAtoBValue.role);

        this.roleArray.push(this.userDetailAtoBValue.role[i].roleName)
      }
      console.log(this.roleArray,this.roleArray);

      if(this.roleArray.includes('Sales'))
      {
        this.assignToHideForSales=false;
        // this.addEditForm.get('userId')?.clearValidators
        // this.addEditForm.get('userId')?.updateValueAndValidity;

        this.addEditForm.get('userId')?.patchValue(this.userDetailAtoBValue.id)
      }
    }

    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      setTimeout(() => {
        this.getSingleData(this.queryParamData.id)
      }, 500);

    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
    }

    this.addEditForm.get('city')?.disable();
    //this.addEditForm.get('postalCode')?.disable();

  }

  getCountryData(){
    this.countriesService.getCountriesList().subscribe(res => {
      console.log(res, "country Name");
      this.countryName=res.data;
    });
  }
  getStateData(){
    this.statesService.getStatesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.StateName=res;
    });
  }
  getCityData(){
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "citiesService Name");
      this.CityName=res.data;
    });
  }
  getPincodeData(){
    this.pincodeService.getPincodeList().subscribe(res => {
      console.log(res, "pincodeService Name");
      this.PincodeName=res;
    });
  }
  getLeadSourceData(){
    this.leadSourceService.getLeadSourceList().subscribe(res => {
      console.log(res, "LeadSourceName Name");
      this.LeadSourceName=res.data;
    });
  }
  getSingleData(id:any){
    this.leadService.getLeadById(id).subscribe(res => {
      console.log(res, "getLeadById");
      this.addEditForm.patchValue(res);
    //  this.countryPlaceHolder='vaibhav'
    //  console.log(Number(res.country),this.countryName);

      // const counNameData= this.countryName.filter(resCountry=>{resCountry.id})
      //const forValue=''


      // for(let i=0;i<this.countryName.length;i++){
      //   if(this.countryName[i].id == res.country)
      //   {
      //       console.log(this.countryName[i].countryName);
      //       this.countryPlaceHolder=this.countryName[i].countryName
      //   }
      // }
      // for(let i=0;i<this.CityName.length;i++){
      //   if(this.CityName[i].id == res.city)
      //   {
      //       console.log(this.CityName[i].cityName);
      //       this.cityPlaceHolder=this.CityName[i].cityName
      //   }
      // }
      // for(let i=0;i<this.StateName.length;i++){
      //   if(this.StateName[i].id == res.state)
      //   {
      //       console.log(this.StateName[i].stateName);
      //       this.satetPlaceHolder=this.StateName[i].stateName
      //   }
      // }
      // for(let i=0;i<this.PincodeName.length;i++){
      //   if(this.PincodeName[i].id == res.postalCode)
      //   {
      //       console.log(this.PincodeName[i].pincode);
      //       this.pincodePlaceHolder=this.PincodeName[i].pincode
      //   }
      // }
      // for(let i=0;i<this.LeadSourceName.length;i++){
      //   if(this.LeadSourceName[i].id == res.leadSource)
      //   {
      //       console.log(this.LeadSourceName[i].leadSourceName);
      //       this.leadSourcePlaceHolder=this.LeadSourceName[i].leadSourceName
      //   }
      // }





      // this.placeHolderSetValue(this.countryName,this.countryPlaceHolder,res.country,'countryName');
      // this.placeHolderSetValue(this.CityName,this.cityPlaceHolder,res.city,'cityName');
      // this.placeHolderSetValue(this.StateName,this.satetPlaceHolder,res.state,'stateName');
      // this.placeHolderSetValue(this.PincodeName,this.pincodePlaceHolder,res.postalCode,'pincode');
      // this.placeHolderSetValue(this.LeadSourceName,this.leadSourcePlaceHolder,res.leadSource,'leadSourceName');

//      this.addEditForm.get('country')?.patchValue(res.country)

      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('salutation')?.patchValue(res.salutation)
      // this.addEditForm.get('firstName')?.patchValue(res.firstName)
      // this.addEditForm.get('city')?.patchValue(res.city)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)
      // this.addEditForm.get('id')?.patchValue(res.id)

      // id:[''],
      // salutation: ['', Validators.compose([Validators.required])],
      // firstName: ['', Validators.compose([Validators.required])],
      // lastName: ['', Validators.compose([Validators.required])],
      // mobileNo: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      // email: ['', Validators.compose([Validators.required, Validators.email])],
      // dateOfBirth: ['', Validators.compose([Validators.required])],
      // gender: ['', Validators.compose([Validators.required])],
      // pan: ['', Validators.compose([Validators.required])],
      // aadhar: ['', Validators.compose([Validators.required])],
      // city: ['', Validators.compose([Validators.required])],
      // country: ['', Validators.compose([Validators.required])],
      // state: ['', Validators.compose([Validators.required])],
      // postalCode: ['',Validators.compose([Validators.required])],
      // leadSource: ['', Validators.compose([Validators.required])],
      // leadStatus: ['', Validators.compose([Validators.required])],
      // userId: ['', Validators.compose([Validators.required])],
    });
  }


  placeHolderSetValue(arrayname:any,placeholdername:any,id:any,objName:any){

    for(let i=0;i<arrayname.length;i++){
      if(arrayname[i].id == id)
      {
          console.log(arrayname[i].countryName);
          placeholdername=arrayname[i].objName
      }
    }
  }
  //selectyion change

  countrySelect(id:any)
  {

     console.log(id);
    this.addEditForm.get('state')?.enable();
    this.countriesService.getStateListByCountryId(id).subscribe(res => {
      console.log(res, "getStateListByCountryId Name");
      this.StateName=res;


    });
    this.cityPlaceHolder='Select City'
    this.satetPlaceHolder='Select State'
    this.pincodePlaceHolder='Select Pincode'
    this.AsssinedToPlaceHolder='Select Asssined To'
  }
  stateSelect(id:any)
  {
    console.log(id);
      this.addEditForm.get('city')?.enable();
      this.statesService.getCityListByStateId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.CityName=res;
      });
      this.satetPlaceHolder='Select State'
      this.pincodePlaceHolder='Select Pincode'
      this.AsssinedToPlaceHolder='Select Asssined To'
  }
  citySelect(id:any)
  {
    console.log(id);
      this.addEditForm.get('postalCode')?.enable();
      this.citiesService.getPincodeListByCityId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.PincodeName=res;
      });
      this.pincodePlaceHolder='Select Pincode'
      this.AsssinedToPlaceHolder='Select Asssined To'
  }

  pincodeSelect(id:any)
  {
    console.log(id);
    this.addEditForm.get('userId')?.enable();
      this.pincodeService.getUserByPincodeId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.AssignToName=res;
      });
    this.AsssinedToPlaceHolder='Select Asssined To'
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/enquiry', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    console.log(this.addEditForm);

    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this.addEditForm.get('leadStatus')?.patchValue('New');
    const _addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.leadService.updateLeadById(this.queryParamData.id,_addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('Enquiry Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/enquiry', { skipLocationChange: true });
      });
    }
    else{
      this.leadService.createLead(_addEditFormData).subscribe(res => {
        console.log(res, "res");
       // this.autoAssignUserByRoleLead()
       this.toastr.success('Enquiry Created Successfully','', { timeOut: 2000 });
       this.router.navigateByUrl('home/enquiry', { skipLocationChange: true });
      });
    }


  }

  autoAssignUserByRoleLead(){
    this.leadService.autoAssignUserByRole(this.userDetailAtoBValue.id,this.roleArray).subscribe(res => {
      console.log(res, "res");
    });
  }

}
