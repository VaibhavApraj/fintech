import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { LeadSourceElement } from 'src/app/core/lead-source/models/leadSource.model';
import { LeadSourceService } from 'src/app/core/lead-source/service/leadSource.service';
import { WorkListService } from '../../../../core/work-list/service/work-list.service';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { catchError, tap } from 'rxjs';
import { StatesElement } from 'src/app/core/geography-masters/states/models/states.model';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';
import { LeadStatusElement } from 'src/app/core/lead-status/models/leadStatus.model';
import { LeadStatusService } from 'src/app/core/lead-status/service/leadStatus.service';
@Component({
  selector: 'app-add-edit-work-list',
  templateUrl: './add-edit-work-list.component.html',
  styleUrls: ['./add-edit-work-list.component.css'],
})
export class AddEditWorkListComponent implements OnInit {

  addEditForm: FormGroup;
  genderName = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' }
  ];
  countryName = [
    { value: '1', viewValue: 'India' },
    { value: '2', viewValue: 'USA' },
    { value: '3', viewValue: 'Itali' },
    { value: '4', viewValue: 'Africa' }
  ];
  // LeadStatusName = [
  //   { id: '1', leadStatusName: 'Maharastra' },
  //   { id: '2', leadStatusName: 'Delhi' },
  //   { id: '3', leadStatusName: 'Chennai' },
  //   { id: '4', leadStatusName: 'Gujarat' }
  // ];
  // cityName = [
  //   {value: '1', viewValue: 'Mumabi'},
  //   {value: '2', viewValue: 'Pune'},
  //   {value: '3', viewValue: 'Nashik'},
  //   {value: '4', viewValue: 'Nagpur'}
  // ];
  ConvenientTime = [
    { value: 1, viewValue: 'Morning between 9:00 AM - 11:59 AM' },
    { value: 2, viewValue: 'Afternoon between 12:00 PM - 4:00 PM' },
    { value: 3, viewValue: 'Evening between 04:01 PM - 07:30 PM' }
  ];
  documentUploadType = [
    { value: '1', viewValue: 'Pan Card' },
    { value: '2', viewValue: 'Adhhar Card' },
    { value: '3', viewValue: 'Passport' },
    { value: '4', viewValue: 'Driving Licence' }
  ];

  LeadApprovalArray = [
    { value: '1', viewValue: 'Approved' },
    { value: '2', viewValue: 'Reject' },
    { value: '3', viewValue: 'Under Review' },
  ];

  CityName: CitiesElement[] = [];
  StateName:StatesElement[]=[];
  LeadSourceName: LeadSourceElement[] = [];
  LeadStatusName: LeadStatusElement[]=[];

  queryParamData: any;
  saveBtn: boolean = true;
  createBtn: boolean = true;
  addEditHeadTitle: any;
  crateleadBtnName: string = ''
  urls: any = [];
  fileName: any = [];
  mobileVerifyBtn: boolean = false;
  emailVerifyBtn: boolean = false;
  VerifyBtnDiv: boolean = false;//true;
  leadApprovalScreen: boolean = false;//true;
  RejectReasonSelect: boolean = false;

  spliteRoleName: any;
  makeaRoleArray: any;

  imgUploadFilename: any;
  imgUpoloadFilecode: any;
  imgUpoladFiletype: any;
  imagUploadArray: any = [];

  salesRole: boolean = false;
  adminRole: boolean = false;
  creditRole: boolean = false;

  mobileVerifyText='Verify'
  emailVerifyText='Verify'

  LeadScheduleScreen: boolean = false;
  LeadLoanScreen: boolean = false;
  leadScheduleStatusOnlyView:boolean=true;
  leadApprovedStatusOnlyViewNotEdit:boolean=false
  IFLeadCreateBySeales:any

  getMobileNoFromRes:any
  getEmailNoFromRes:any

  getMobileNoFromResBtnDisable:boolean = false;
  getEmailNoFromResBtnDisabl:boolean = false;
  dateOfBirthFormate:any
  AsssinedToPlaceHolder:any

statePlaceholderName:any
cityPlaceholderName:any
leadSourcePlaceholderName:any
leadStatusPlaceholderName:any='Select Lead Status';
leadStatusLoanPlaceholderName:any='Select Lead Status';
leadStatusApprovedPlaceholderName:any='Select Lead Status';

viewLeadStage:any
ViewleadStatus:any
  constructor( private leadStatusService:LeadStatusService,private statesService:StatesService,private toastr: ToastrService,private fb: FormBuilder, private router: Router, private workListService: WorkListService, private routes: ActivatedRoute, private citiesService: CitiesService, private leadSourceService: LeadSourceService) {

    this.spliteRoleName = sessionStorage.getItem('role');
    this.makeaRoleArray = this.spliteRoleName?.split(',');


    for (let j = 0; j < this.makeaRoleArray.length; j++) {
      console.log(this.makeaRoleArray[j]);

      if (this.makeaRoleArray[j] == 'Sales') {
        //this.VerifyBtnDiv=true;
        this.salesRole = true
      }
      if (this.makeaRoleArray[j] == 'Credit') {
        this.leadApprovalScreen = true;
        this.creditRole = true;
      }
    }

    this.addEditForm = this.fb.group({
      id: [''],
      leadId: [''],
      name: [''],
      mobileNo: [''],
      email: [''],
      dateOfBirth: [''],
      pan: [''],
      aadhar: [''],
      city: [''],
      gender:[''],
      leadSource: [''],
      leadMaintainceStatus: [''],
      assign_To: [''],
      state: [''],
      scheduleDate: [''],
      convenientTime: [''],
      leadScheduleStatus: [''],
      scheduledRemark: [''],

      area:[''],
      loanAmount: [''],
      product: [''],
      tenor: [''],
      remarkLeadLoan: [''],
      // file_upload: this.fb.array([]),
      file_upload: [''],
      CibilScore: [''],
      leadLoanStatus:[''],
      mobileNoVarification: [''],
      emailIdVarification: [''],

      leadStage: [''],
      rejectReason: [''],
      approvedRemark: [''],
      leadApprovedStatus:['']
    })
  }

  ngOnInit(): void {

    this.routes.queryParams.subscribe(res => this.queryParamData = res);

    if (this.queryParamData.type == 'edit') {
      console.log('edit');
      this.saveBtn = true;
      this.createBtn = true;
      this.addEditHeadTitle = 'Edit Worklist';
      this.getDropdownData()
      this.getSingleData(this.queryParamData.id)

      // if(this.IFLeadCreateBySeales != "Sales"){
      //   this.LeadScheduleScreen = true
      //   this.crateleadBtnName = 'Save'
      //   this.saveBtn = false;
      // this.addEditForm.get('scheduleDate')?.setValidators([Validators.required])
      // this.addEditForm.get('scheduleDate')?.updateValueAndValidity
      // this.addEditForm.get('convenientTime')?.setValidators([Validators.required])
      // this.addEditForm.get('convenientTime')?.updateValueAndValidity
      // this.addEditForm.get('scheduledRemark')?.setValidators([Validators.required])
      // this.addEditForm.get('scheduledRemark')?.updateValueAndValidity
      // }
      // else{
      //   this.makeLeadLoanScreenLogic();
      // }

      if (this.leadApprovalScreen == true) {
        this.addEditHeadTitle = 'Lead Approval';
        this.saveBtn = true;
        this.createBtn = true;
        this.crateleadBtnName = 'Submit'
        // this.getSingleData(this.queryParamData.id);



        this.addEditForm.get('leadStage')?.setValidators([Validators.required])
      this.addEditForm.get('leadStage')?.updateValueAndValidity
      this.addEditForm.get('approvedRemark')?.setValidators([Validators.required])
      this.addEditForm.get('approvedRemark')?.updateValueAndValidity
      // this.addEditForm.get('leadApprovedStatus')?.setValidators([Validators.required])
      // this.addEditForm.get('leadApprovedStatus')?.updateValueAndValidity
       // this.crateleadBtnName = 'Verify'

        // this.addEditForm.get('leadStage')?.clearValidators()
        // this.addEditForm.get('leadStage')?.updateValueAndValidity
        // this.addEditForm.get('remark')?.clearValidators()
        // this.addEditForm.get('remark')?.updateValueAndValidity
      }
      // else {
      //   this.crateleadBtnName = 'Submit'
      // }
    } else if (this.queryParamData.type == 'view') {
      this.addEditHeadTitle = 'View Worklist'
      this.saveBtn = false;
      this.createBtn = false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else if (this.leadApprovalScreen == true) {
      // this.addEditHeadTitle = 'Lead Approval';
      // this.saveBtn = true;
      // this.createBtn = true;
      // this.crateleadBtnName = 'Submit'
      // this.getSingleData(this.queryParamData.id);
      // this.addEditForm.get('loanAmount')?.disable()
      // this.addEditForm.get('product')?.disable()
      // this.addEditForm.get('tenor')?.disable()
      // this.addEditForm.get('remarkLeadLoan')?.disable()
      // this.addEditForm.get('file_upload')?.disable()
      // this.addEditForm.get('loanAmount')?.clearValidators()
      // this.addEditForm.get('loanAmount')?.updateValueAndValidity
      // this.addEditForm.get('product')?.clearValidators()
      // this.addEditForm.get('product')?.updateValueAndValidity
      // this.addEditForm.get('tenor')?.clearValidators()
      // this.addEditForm.get('tenor')?.updateValueAndValidity
      // this.addEditForm.get('remarkLeadLoan')?.clearValidators()
      // this.addEditForm.get('remarkLeadLoan')?.updateValueAndValidity
      // this.addEditForm.get('file_upload')?.clearValidators()
      // this.addEditForm.get('file_upload')?.updateValueAndValidity

      // this.addEditForm.get('leadStage')?.setValidators([Validators.required])
      // this.addEditForm.get('leadStage')?.updateValueAndValidity
      // this.addEditForm.get('remark')?.setValidators([Validators.required])
      // this.addEditForm.get('remark')?.updateValueAndValidity
    }
    else {
      this.addEditHeadTitle = 'Create'
      this.crateleadBtnName = 'Create'
      //this.getDropdownData()
    }
  }

  getDropdownData(){
    // this.getStateData();
    // this.getCityData();
    // this.getLeadSourceData();
    this.getLeadStatusData();
  }

  makeLeadLoanScreenLogic(){
    this.LeadLoanScreen = true
    this.crateleadBtnName = 'Verify'
    this.saveBtn = true;
    this.addEditForm.get('loanAmount')?.setValidators([Validators.required])
    this.addEditForm.get('loanAmount')?.updateValueAndValidity
    this.addEditForm.get('product')?.setValidators([Validators.required])
    this.addEditForm.get('product')?.updateValueAndValidity
    this.addEditForm.get('tenor')?.setValidators([Validators.required])
    this.addEditForm.get('tenor')?.updateValueAndValidity
    this.addEditForm.get('remarkLeadLoan')?.setValidators([Validators.required])
    this.addEditForm.get('remarkLeadLoan')?.updateValueAndValidity
    this.addEditForm.get('file_upload')?.setValidators([Validators.required])
    this.addEditForm.get('file_upload')?.updateValueAndValidity
    this.addEditForm.get('leadLoanStatus')?.setValidators([Validators.required])
    this.addEditForm.get('leadLoanStatus')?.updateValueAndValidity

  }

  getCityData() {
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "citiesService Name");
      this.CityName = res.data;
    });
  }
  getLeadSourceData() {
    this.leadSourceService.getLeadSourceList().subscribe(res => {
      console.log(res, "LeadSourceName Name");
      this.LeadSourceName = res.data;
    });
  }
  getLeadStatusData(){
    this.leadStatusService.getLeadStatusList().subscribe(res => {
      console.log(res, "getLeadStatusList Name");
      this.LeadStatusName = res.data;
    });
  }
  getStateData(){
    this.statesService.getStatesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.StateName=res;
    });
  }

  getSingleData(id: any) {
    // debugger
    this.workListService.getLeadById(id).subscribe(res => {
      console.log(res.data, "getWorklistById");
      this.viewLeadStage=res.data.leadStage
      this.ViewleadStatus=res.data.leadStatus
      this.addEditForm.get('area')?.patchValue(res.data.pincode.areaName)
      this.AsssinedToPlaceHolder=res.data.assignTo.firstName+ " "+res.data.assignTo.lastName
      this.statePlaceholderName=res.data.state.stateName
      this.cityPlaceholderName=res.data.city.cityName
      if(res.data.leadSource!=null)
      this.leadSourcePlaceholderName=res.data.leadSource.leadSourceName

      this.addEditForm.patchValue(res.data)
      this.IFLeadCreateBySeales=res.data.createdBy
      this.getMobileNoFromRes=res.data.mobileNo
      this.getEmailNoFromRes=res.data.email
      this.dateOfBirthFormate=res.data.dateOfBirth
      if(res.data.user!=null){
        this.addEditForm.get('userId')?.patchValue(res.data.user.id)
        this.AsssinedToPlaceHolder=res.data.user.firstName +" "+res.data.user.lastName;
      }
      if(this.IFLeadCreateBySeales != "Sales" && res.data.leadStage=='New'){
        this.LeadScheduleScreen = true
        this.crateleadBtnName = 'Save'
        this.saveBtn = false;

      this.addEditForm.get('scheduleDate')?.setValidators([Validators.required])
      this.addEditForm.get('scheduleDate')?.updateValueAndValidity
      this.addEditForm.get('convenientTime')?.setValidators([Validators.required])
      this.addEditForm.get('convenientTime')?.updateValueAndValidity
      this.addEditForm.get('scheduledRemark')?.setValidators([Validators.required])
      this.addEditForm.get('scheduledRemark')?.updateValueAndValidity
      this.addEditForm.get('leadScheduleStatus')?.setValidators([Validators.required])
      this.addEditForm.get('leadScheduleStatus')?.updateValueAndValidity
      this.addEditForm.get('leadScheduleStatus')?.patchValue(res.data.leadStatus.id)
      }
      else if(this.IFLeadCreateBySeales == "Sales" && res.data.leadStage=='New'){
        this.makeLeadLoanScreenLogic();
        this.leadScheduleStatusOnlyView=false
        this.addEditForm.get('leadLoanStatus')?.patchValue(res.data.leadStatus.id)
      }
      else if(res.data.leadStage=='Scheduled'){
        this.LeadScheduleScreen = true

        this.addEditForm.get('scheduleDate')?.disable()
        this.addEditForm.get('convenientTime')?.disable()
        this.addEditForm.get('scheduledRemark')?.disable()
        this.addEditForm.get('leadScheduleStatus')?.disable()
        this.leadStatusPlaceholderName=res.data.leadStatus.leadStatus
        this.leadScheduleStatusOnlyView=false
        this.makeLeadLoanScreenLogic();
        this.addEditForm.get('leadLoanStatus')?.patchValue(res.data.leadStatus.id)
      }
      const fullName = res.data.firstName + " " + res.data.lastName
      this.addEditForm.get('name')?.patchValue(fullName);
      this.addEditForm.get('leadId')?.patchValue(res.data.leadId);
      this.addEditForm.get('remarkLeadLoan')?.patchValue(res.data.loanRemark);
      this.addEditForm.get('leadMaintainceStatus')?.patchValue(res.data.leadStage);
      // this.addEditForm.get('assign_To')?.patchValue('');
      //this.leadStatusLoanPlaceholderName=res.data.leadStage

      if(res.data.tenor==0)
      this.addEditForm.get('tenor')?.patchValue('')

      if(res.data.loanAmount==0)
      this.addEditForm.get('loanAmount')?.patchValue('')

      if(res.data.convenientTime==0)
      this.addEditForm.get('convenientTime')?.patchValue('')

      this.addEditForm.get('city')?.disable()
      this.addEditForm.get('gender')?.disable()
      this.addEditForm.get('state')?.disable()
      this.addEditForm.get('area')?.disable()
      this.addEditForm.get('dateOfBirth')?.disable()
      this.addEditForm.get('leadId')?.disable()
      this.addEditForm.get('name')?.disable()
      this.addEditForm.get('mobileNo')?.disable()
      this.addEditForm.get('email')?.disable()
      this.addEditForm.get('pan')?.disable()
      this.addEditForm.get('aadhar')?.disable()
      this.addEditForm.get('leadSource')?.disable()
      this.addEditForm.get('leadMaintainceStatus')?.disable()
      // this.addEditForm.get('assign_To')?.disable()
      // this.addEditForm.get('scheduleDate')?.disable()
      // this.addEditForm.get('convenientTime')?.disable()
      // this.addEditForm.get('scheduledRemark')?.disable()

      if (this.leadApprovalScreen == true) {
        if(this.IFLeadCreateBySeales != "Sales" && res.data.leadStage=='Under process'){
          this.LeadScheduleScreen = true
        }else  if(this.IFLeadCreateBySeales == "Sales" && res.data.leadStage=='Under process'){
          this.LeadLoanScreen = true
        }
        //this.crateleadBtnName = 'Submit';
        this.LeadLoanScreen = true
        if(this.addEditForm.get('leadStage')?.value == 'Under process'){
          this.leadStatusLoanPlaceholderName=res.data.leadStatus.leadStatus
          this.addEditForm.get('leadStage')?.patchValue('')
          this.addEditForm.get('remarkLeadLoan')?.patchValue(res.data.loanRemark)
        }

        this.addEditForm.get('scheduleDate')?.disable()
        this.addEditForm.get('convenientTime')?.disable()
        this.addEditForm.get('scheduledRemark')?.disable()
        this.addEditForm.get('leadScheduleStatus')?.disable()
        this.leadScheduleStatusOnlyView=false
        this.addEditForm.get('loanAmount')?.disable()
        this.addEditForm.get('product')?.disable()
        this.addEditForm.get('tenor')?.disable()
        this.addEditForm.get('remarkLeadLoan')?.disable()
        this.addEditForm.get('file_upload')?.disable()
        this.addEditForm.get('leadLoanStatus')?.disable()

        this.addEditForm.get('leadStage')?.setValidators([Validators.required])
        this.addEditForm.get('leadStage')?.updateValueAndValidity
        this.addEditForm.get('approvedRemark')?.setValidators([Validators.required])
        this.addEditForm.get('approvedRemark')?.updateValueAndValidity
        //     this.addEditForm.get('leadApprovedStatus')?.setValidators([Validators.required])
        // this.addEditForm.get('leadApprovedStatus')?.updateValueAndValidity
      }
      if (this.queryParamData.type == 'view'){
        if(res.data.scheduleDate == null){
          this.LeadScheduleScreen = false
        }
        if(res.data.product == null){
          this.LeadLoanScreen = false

        }
        if(res.data.approvedRemark == null){
          this.leadApprovalScreen=false
          //this.leadScheduleStatusOnlyView=false
        }
        if(res.data.leadStage=='Scheduled'){
          this.leadScheduleStatusOnlyView=true
        }
        this.saveBtn = false;
        this.leadStatusLoanPlaceholderName=res.data.leadStatus.leadStatus
        this.leadApprovedStatusOnlyViewNotEdit=true
      }
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm() {
    this.router.navigateByUrl('home/worklist', { skipLocationChange: true });
  }
  saveAddEditForm() {

  }
  createAddEditForm() {
    console.log(this.addEditForm)
   //  debugger


    console.log(this.addEditForm.value)


    // if (this.addEditForm.invalid) {
    //   this.addEditForm.markAllAsTouched()
    //   return;
    // }
    //if(!this.leadApprovalScreen){
    if ((this.salesRole == true && this.creditRole == true) || this.salesRole == true) {
      if(this.LeadLoanScreen == false){
        if (this.addEditForm.invalid) {
          // this.addEditForm.markAllAsTouched()
          this.addEditForm.get('scheduleDate')?.markAsTouched()
          this.addEditForm.get('convenientTime')?.markAsTouched()
          this.addEditForm.get('scheduledRemark')?.markAsTouched()
          this.addEditForm.get('leadScheduleStatus')?.markAsTouched()
          return;
        }
        const _addEditFormData = this.addEditForm.value;
        if (this.queryParamData.type == 'edit') {
          this.workListService.updateLeadmaintance(this.queryParamData.id, _addEditFormData).subscribe(res => {
            console.log(res, "edit updateLeadmaintance");
            this.toastr.success('Lead Scheduled Details Updated Successfully','', { timeOut: 2000 });
            this.router.navigateByUrl('home/worklist', { skipLocationChange: true });
          this.addEditForm.get('scheduleDate')?.disable()
          this.addEditForm.get('convenientTime')?.disable()
          this.addEditForm.get('scheduledRemark')?.disable()
          this.addEditForm.get('leadScheduleStatus')?.disable()
          });
        }
        this.makeLeadLoanScreenLogic();
      }

      else if (this.VerifyBtnDiv==true) {

        if (this.addEditForm.invalid) {
          this.addEditForm.markAllAsTouched()
          return;
        }
        this.addEditForm.get('leadStage')?.patchValue('Under Process');
        const _addEditFormData = this.addEditForm.value;
        _addEditFormData.file_upload=this.imagUploadArray
        _addEditFormData.loanAmount=this.addEditForm.get('loanAmount')?.value
        _addEditFormData.product=this.addEditForm.get('product')?.value
        _addEditFormData.tenor=this.addEditForm.get('tenor')?.value
        _addEditFormData.remarkLeadLoan=this.addEditForm.get('remarkLeadLoan')?.value
        _addEditFormData.leadLoanStatus=this.addEditForm.get('leadLoanStatus')?.value
        console.log(this.addEditForm)

        if (this.queryParamData.type == 'edit' && this.emailVerifyBtn == true && this.mobileVerifyBtn == true) {
          this.workListService.updateWorklist(this.queryParamData.id, _addEditFormData).subscribe(res => {
            console.log(res, "edit");
            this.toastr.success('Lead Loan Details Updated Successfully','', { timeOut: 2000 });
            this.router.navigateByUrl('home/worklist', { skipLocationChange: true });
          });
        }
        else {
          this.toastr.error('Please Verify','', { timeOut: 2000 });

          if(this.mobileVerifyBtn == false)
          this.addEditForm.get('mobileNoVarification')?.setErrors({'incorrect': true});

          if(this.emailVerifyBtn == false)
          this.addEditForm.get('emailIdVarification')?.setErrors({'incorrect': true});
        }
      }
      else {
        if (this.addEditForm.invalid) {
          this.addEditForm.get('loanAmount')?.markAsTouched()
          this.addEditForm.get('product')?.markAsTouched()
          this.addEditForm.get('tenor')?.markAsTouched()
          this.addEditForm.get('remarkLeadLoan')?.markAsTouched()
          this.addEditForm.get('file_upload')?.markAsTouched()
          this.addEditForm.get('leadLoanStatus')?.markAsTouched()
          return;
        }

        for (let j = 0; j < this.urls.length; j++) {
          console.log(this.fileName);
          this.imagUploadArray.push({ 'imageName': this.fileName[j], 'fileData': this.urls[j] })
        }
        console.log(this.imagUploadArray);

        this.VerifyBtnDiv = true;
        this.crateleadBtnName = 'Submit';
        this.addEditForm.get('scheduleDate')?.disable()
        this.addEditForm.get('convenientTime')?.disable()
        this.addEditForm.get('scheduledRemark')?.disable()
        this.addEditForm.get('leadScheduleStatus')?.disable()
        this.addEditForm.get('loanAmount')?.disable()
        this.addEditForm.get('product')?.disable()
        this.addEditForm.get('tenor')?.disable()
        this.addEditForm.get('remarkLeadLoan')?.disable()
        this.addEditForm.get('file_upload')?.disable()
        this.addEditForm.get('leadLoanStatus')?.disable()
        this.addEditForm.get('mobileNoVarification')?.setValidators([Validators.required])
        this.addEditForm.get('mobileNoVarification')?.updateValueAndValidity
        this.addEditForm.get('emailIdVarification')?.setValidators([Validators.required])
        this.addEditForm.get('emailIdVarification')?.updateValueAndValidity
        // this.addEditForm.get('leadApprovedStatus')?.setValidators([Validators.required])
        // this.addEditForm.get('leadApprovedStatus')?.updateValueAndValidity


        this.workListService.mobileNoVarification(this.queryParamData.id, this.getMobileNoFromRes).subscribe(res => {
          console.log(res, "edit");
          this.toastr.success(res.message,'', { timeOut: 2000 });
        });
      }
    }
    //
    // if(this.leadApprovalScreen){
    else if (this.creditRole == true) {
      this.checkvalidation('leadStage', this.addEditForm.get('leadStage')?.value)
      // this.checkvalidation('remark', this.addEditForm.get('remark')?.value)
      if (this.addEditForm.invalid) {
        this.addEditForm.markAllAsTouched()
        return;
      }
      // this.addEditForm.get('leadStage')?.setValidators([Validators.required])
      // this.addEditForm.get('leadStage')?.updateValueAndValidity
      // this.addEditForm.get('remark')?.setValidators([Validators.required])
      // this.addEditForm.get('remark')?.updateValueAndValidity

      const _addEditFormData = this.addEditForm.value;
      console.log(this.addEditForm)


      if (this.queryParamData.type == 'edit') {
        this.workListService.updateLeadApproval(this.queryParamData.id, _addEditFormData).subscribe(res => {
          console.log(res, "edit");
          this.toastr.success('Lead Approval Details Updated Successfully','', { timeOut: 2000 });
          this.router.navigateByUrl('home/worklist', { skipLocationChange: true });
        });
      }

    }
  }

  onSelectFile(event: any) {
    console.log(event, "event")
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        this.fileName.push(event.target.files[i].name);
        this.imgUploadFilename = event.target.files[i].name;
        this.imgUpoladFiletype = event.target.files[i].type;
        var reader = new FileReader();
        reader.onload = (event: any) => {
          console.log(event, " name");
          console.log(event.target.result);
          this.urls.push(event.target.result);
          this.imgUpoloadFilecode = event.target.result
        }

        //                 let fileArrayJsonData={"fileName":event.target.files[i].name,"fileType":event.target.files[i].type,"fileData":this.imgUpoloadFilecode}
        // console.log(fileArrayJsonData);

        //                 this.imagUploadArray.push(fileArrayJsonData)

        reader.readAsDataURL(event.target.files[i]);
        console.log(this.imgUpoloadFilecode, this.urls[i], this.imgUploadFilename, this.imgUpoladFiletype);
        //                this.imagUploadArray
      }
      console.log(this.imgUpoloadFilecode, this.urls, this.imgUploadFilename, this.imgUpoladFiletype);
      console.log(this.imagUploadArray);

    }
    console.log(this.fileName, " this.fileName");
    // const fileSize=this.fileName.length;
    // this.addEditForm.get('file_upload')?.patchValue(fileSize)


  }

  removeFile(id: any) {
    console.log(id);
    this.fileName.splice(id, 1);
    this.urls.splice(id, 1);
    // console.log(this.fileName.length);
    // const fileSize=this.fileName.length;
    // this.addEditForm.get('file_upload')?.patchValue(fileSize)
  }
  addMoreImage() {
    const addMoreImage = this.addEditForm.get('file_upload') as FormArray;
    addMoreImage.push(this.fb.group({
      type: "",
      file: "",
      name: "",
    }))
  }

  //varification
  mobileVerify() {
    if (this.addEditForm.get('mobileNoVarification')?.invalid) {
      this.addEditForm.get('mobileNoVarification')?.markAsTouched()
      return;
    }
   this.workListService.mobileNoOTPVarification(this.queryParamData.id, this.addEditForm.get('mobileNoVarification')?.value).subscribe(res => {
     console.log(res, "edit");
      if(res.msgKey=='Success'){
        this.mobileVerifyBtn = true;
        this.mobileVerifyText='Verified';
        this.getMobileNoFromResBtnDisable=true
        this.addEditForm.get('mobileNoVarification')?.disable()
        this.toastr.success(res.message,'', { timeOut: 2000 });
      }else{
        this.toastr.error(res.message,'', { timeOut: 2000 });
        // this.addEditForm.get('mobileNoVarification')?.setValidators([Validators.required])
        // this.addEditForm.get('mobileNoVarification')?.updateValueAndValidity
        this.addEditForm.get('mobileNoVarification')?.setErrors({'incorrect': true});
      }

    });

  }
  emailVerify() {
    // this.workListService.emailIOTPVarification(this.queryParamData.id, this.getEmailNoFromRes).subscribe(res => {
    //   console.log(res, "edit");
      this.emailVerifyBtn = true;
    this.emailVerifyText='Verified'
    this.getEmailNoFromResBtnDisabl=true
    this.addEditForm.get('emailIdVarification')?.disable()
    // });

  }

  //LeadApprovalChange if reject
  LeadApprovalChange(event: any) {
    console.log(event);

    if (event == 'Reject') {
      this.RejectReasonSelect = true;
      this.addEditForm.get('rejectReason')?.setValidators([Validators.required])
      this.addEditForm.get('rejectReason')?.updateValueAndValidity
    }
    else {
      this.RejectReasonSelect = false;
      this.addEditForm.get('rejectReason')?.clearValidators()
      this.addEditForm.get('rejectReason')?.updateValueAndValidity
    }
  }

  checkvalidation(contronname: any, value: any) {
    if (value == "Generated"){
      this.addEditForm.get(contronname)?.setErrors({'incorrect': true})
      this.addEditForm.get(contronname)?.updateValueAndValidity
    }
  }
}
