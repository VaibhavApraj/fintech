import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { LeadSourceElement } from 'src/app/core/lead-source/models/leadSource.model';
import { LeadSourceService } from 'src/app/core/lead-source/service/leadSource.service';
import { ManualAssignmentService } from 'src/app/core/manual-assignment/service/manual-assignment.service';
import { UserService } from 'src/app/core/user/service/user.service';

@Component({
  selector: 'app-add-edit-manual-assignment',
  templateUrl: './add-edit-manual-assignment.component.html',
  styleUrls: ['./add-edit-manual-assignment.component.css']
})
export class AddEditManualAssignmentComponent implements OnInit {

  addEditForm: FormGroup;
  genderName = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' }
  ];
  countryName = [
    { value: '1', viewValue: 'India' },
    { value: '2', viewValue: 'USA' },
    { value: '3', viewValue: 'Itali' },
    { value: '4', viewValue: 'Africa' }
  ];
  stateName = [
    { value: '1', viewValue: 'Maharastra' },
    { value: '2', viewValue: 'Delhi' },
    { value: '3', viewValue: 'Chennai' },
    { value: '4', viewValue: 'Gujarat' }
  ];
  // cityName = [
  //   {value: '1', viewValue: 'Mumabi'},
  //   {value: '2', viewValue: 'Pune'},
  //   {value: '3', viewValue: 'Nashik'},
  //   {value: '4', viewValue: 'Nagpur'}
  // ];
  ConvenientTime = [
    { value: 1, viewValue: 'Morning between 9:00 AM - 11:59 AM' },
    { value: 2, viewValue: 'Afternoon between 12:00 PM - 4:00 PM' },
    { value: 3, viewValue: 'Evening between 04:01 PM - 07:30 PM' }
  ];
  documentUploadType = [
    { value: '1', viewValue: 'Pan Card' },
    { value: '2', viewValue: 'Adhhar Card' },
    { value: '3', viewValue: 'Passport' },
    { value: '4', viewValue: 'Driving Licence' }
  ];

  LeadApprovalArray = [
    { value: '1', viewValue: 'Approved' },
    { value: '2', viewValue: 'Reject' },
    { value: '3', viewValue: 'Under Review' },
  ];

  CityName: CitiesElement[] = [];
  LeadSourceName: LeadSourceElement[] = [];

  queryParamData: any;
  saveBtn: boolean = true;
  createBtn: boolean = true;
  addEditHeadTitle: any;
  crateleadBtnName: string = ''
  urls: any = [];
  fileName: any = [];
  mobileVerifyBtn: boolean = false;
  emailVerifyBtn: boolean = false;
  VerifyBtnDiv: boolean = false;//true;
  leadApprovalScreen: boolean = false;//true;
  RejectReasonSelect: boolean = false;

  spliteRoleName: any;
  makeaRoleArray: any;

  imgUploadFilename: any;
  imgUpoloadFilecode: any;
  imgUpoladFiletype: any;
  imagUploadArray: any = [];

  salesRole: boolean = false;
  adminRole: boolean = false;
  creditRole: boolean = false;

  AssignToName:any=[];

  constructor(private toastr: ToastrService,private userService: UserService,private fb: FormBuilder, private router: Router, private manualAssignmentService: ManualAssignmentService, private routes: ActivatedRoute, private citiesService: CitiesService, private leadSourceService: LeadSourceService) {

    this.spliteRoleName = sessionStorage.getItem('role');
    this.makeaRoleArray = this.spliteRoleName?.split(',');


    for (let j = 0; j < this.makeaRoleArray.length; j++) {
      console.log(this.makeaRoleArray[j]);

      if (this.makeaRoleArray[j] == 'Admin') {
        this.VerifyBtnDiv = true;
        this.leadApprovalScreen = true;
        this.adminRole = true;
      }
      if (this.makeaRoleArray[j] == 'Sales') {
        //this.VerifyBtnDiv=true;
        this.salesRole = true
      }
      if (this.makeaRoleArray[j] == 'Credit') {
        this.leadApprovalScreen = true;
        this.creditRole = true;
      }
    }

    this.addEditForm = this.fb.group({
      id: [''],
      leadId: [''],
      name: [''],
      mobileNo: [''],
      email: [''],
      dateOfBirth: [''],
      pan: [''],
      aadhar: [''],
      city: [''],
      leadSource: [''],
      leadMaintainceStatus: [''],
      assign_To: [''],
      assignment:['']
      // scheduleDate: ['', Validators.compose([Validators.required])],
      // convenientTime: ['', Validators.compose([Validators.required])],
      // scheduledRemark: ['', Validators.compose([Validators.required])],

      // loanAmount: ['', Validators.compose([Validators.required])],
      // product: ['', Validators.compose([Validators.required])],
      // tenor: ['', Validators.compose([Validators.required])],
      // scheduledRemarkLoan: ['', Validators.compose([Validators.required])],
      // file_upload: [''],
      // CibilScore: [''],
      // mobileNoVarification: [''],
      // emailIdVarification: [''],
      // leadStatus: ['', Validators.compose([Validators.required])],
      // rejectReason: [''],
      // Remark: ['', Validators.compose([Validators.required])],

    })
  }

  ngOnInit(): void {
    this.getCityData();
    this.getLeadSourceData();
    this. getAllUserForManualAssgment()

    this.routes.queryParams.subscribe(res => this.queryParamData = res);
    if (this.queryParamData.type == 'edit') {
      console.log('edit');

      this.saveBtn = true;
      this.createBtn = true;
      this.addEditHeadTitle = 'Edit';
      // if (this.leadApprovalScreen == false) {
      //   this.crateleadBtnName = 'Verify'

      //   this.addEditForm.get('leadStatus')?.clearValidators()
      //   this.addEditForm.get('leadStatus')?.updateValueAndValidity
      //   this.addEditForm.get('Remark')?.clearValidators()
      //   this.addEditForm.get('Remark')?.updateValueAndValidity
      // } else {
        this.crateleadBtnName = 'Update'
      // }
      this.getSingleData(this.queryParamData.id)
    } else if (this.queryParamData.type == 'view') {
      this.addEditHeadTitle = 'View'
      this.saveBtn = false;
      this.createBtn = false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else if (this.leadApprovalScreen == true) {
      this.addEditHeadTitle = 'Lead Approval';
      this.saveBtn = true;
      this.createBtn = true;
      this.crateleadBtnName = 'Submit'
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.get('loanAmount')?.disable()
      this.addEditForm.get('product')?.disable()
      this.addEditForm.get('tenor')?.disable()
      this.addEditForm.get('remarkLeadLoan')?.disable()
      this.addEditForm.get('file_upload')?.disable()
      this.addEditForm.get('loanAmount')?.clearValidators()
      this.addEditForm.get('loanAmount')?.updateValueAndValidity
      this.addEditForm.get('product')?.clearValidators()
      this.addEditForm.get('product')?.updateValueAndValidity
      this.addEditForm.get('tenor')?.clearValidators()
      this.addEditForm.get('tenor')?.updateValueAndValidity
      this.addEditForm.get('remarkLeadLoan')?.clearValidators()
      this.addEditForm.get('remarkLeadLoan')?.updateValueAndValidity
      this.addEditForm.get('file_upload')?.clearValidators()
      this.addEditForm.get('file_upload')?.updateValueAndValidity
      // this.addEditForm.get('leadStatus')?.setValidators([Validators.required])
      // this.addEditForm.get('leadStatus')?.updateValueAndValidity
      // this.addEditForm.get('Remark')?.setValidators([Validators.required])
      // this.addEditForm.get('Remark')?.updateValueAndValidity
    }
    else {
      this.addEditHeadTitle = 'Edit'
      this.crateleadBtnName = 'Update'
    }
  }

  getCityData() {
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "citiesService Name");
      this.CityName = res.data;
    });
  }
  getLeadSourceData() {
    this.leadSourceService.getLeadSourceList().subscribe(res => {
      console.log(res, "LeadSourceName Name");
      this.LeadSourceName = res.data;
    });
  }

  getAllUserForManualAssgment(){
      this.userService.getUserList().subscribe(res => {
        console.log(res, "getAllUserForManualAssgment");
        this.AssignToName=res;
      });
  }
  getSingleData(id: any) {
    this.manualAssignmentService.getLeadById(id).subscribe(res => {
      console.log(res, "getLeadById");
      this.addEditForm.patchValue(res)
      const fullName = res.salutation + " " + res.firstName + " " + res.lastName
      this.addEditForm.get('name')?.patchValue(fullName);
      this.addEditForm.get('leadId')?.patchValue(res.leadId);

      // this.addEditForm.get('leadMaintainceStatus')?.patchValue('');
      // this.addEditForm.get('assign_To')?.patchValue('');

      this.addEditForm.get('city')?.disable()
      this.addEditForm.get('dateOfBirth')?.disable()
      this.addEditForm.get('leadId')?.disable()
      this.addEditForm.get('name')?.disable()
      this.addEditForm.get('mobileNo')?.disable()
      this.addEditForm.get('email')?.disable()
      this.addEditForm.get('pan')?.disable()
      this.addEditForm.get('aadhar')?.disable()
      this.addEditForm.get('leadSource')?.disable()
      this.addEditForm.get('leadMaintainceStatus')?.disable()
      // this.addEditForm.get('assign_To')?.disable()
      // this.addEditForm.get('scheduleDate')?.disable()
      // this.addEditForm.get('convenientTime')?.disable()
      // this.addEditForm.get('scheduledRemark')?.disable()

      if (this.leadApprovalScreen == true) {
        this.addEditForm.get('scheduleDate')?.disable()
        this.addEditForm.get('convenientTime')?.disable()
        this.addEditForm.get('scheduledRemark')?.disable()

        this.addEditForm.get('loanAmount')?.disable()
        this.addEditForm.get('product')?.disable()
        this.addEditForm.get('tenor')?.disable()
        this.addEditForm.get('remarkLeadLoan')?.disable()
        this.addEditForm.get('file_upload')?.disable()

        this.addEditForm.get('leadStatus')?.setValidators([Validators.required])
        this.addEditForm.get('leadStatus')?.updateValueAndValidity
        this.addEditForm.get('approvedRemark')?.setValidators([Validators.required])
        this.addEditForm.get('approvedRemark')?.updateValueAndValidity
      }
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm() {
    this.router.navigateByUrl('home/manual-assignment', { skipLocationChange: true });
  }
  saveAddEditForm() {

  }
  createAddEditForm() {
    console.log(this.addEditForm)
    debugger
    for (let j = 0; j < this.urls.length; j++) {
      console.log(this.fileName);

      this.imagUploadArray.push({ 'imageName': this.fileName[j], 'fileData': this.urls[j] })
    }
    console.log(this.imagUploadArray);

    console.log(this.addEditForm.value)
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    //if(!this.leadApprovalScreen){
    if ((this.salesRole == true && this.creditRole == true) || this.salesRole == true) {
      if (this.VerifyBtnDiv) {

        if (this.addEditForm.invalid) {
          this.addEditForm.markAllAsTouched()
          return;
        }
        this.addEditForm.get('leadStatus')?.patchValue('Under Process');
        const _addEditFormData = this.addEditForm.value;
        console.log(this.addEditForm)

        if (this.queryParamData.type == 'edit') {
          this.manualAssignmentService.updateWorklist(this.queryParamData.id, _addEditFormData).subscribe(res => {
            console.log(res, "edit");
            this.toastr.success('Lead Loan Updated Successfully','', { timeOut: 2000 });
          });
        }
        else {
          this.manualAssignmentService.createLead(_addEditFormData).subscribe(res => {
            console.log(res, "view");
            this.toastr.success('Lead Loan Created Successfully','', { timeOut: 2000 });
          });
        }
        this.router.navigateByUrl('home/manual-assignment', { skipLocationChange: true });
      }
      else {
        this.VerifyBtnDiv = true;
        this.crateleadBtnName = 'Submit';
        this.addEditForm.get('mobileNoVarification')?.setValidators([Validators.required])
        this.addEditForm.get('mobileNoVarification')?.updateValueAndValidity
        this.addEditForm.get('emailIdVarification')?.setValidators([Validators.required])
        this.addEditForm.get('emailIdVarification')?.updateValueAndValidity
      }
    }
    //
    // if(this.leadApprovalScreen){
    if (this.creditRole == true) {
      this.checkvalidation('leadStatus', this.addEditForm.get('leadStatus')?.value)
      // this.checkvalidation('Remark', this.addEditForm.get('Remark')?.value)
      if (this.addEditForm.invalid) {
        this.addEditForm.markAllAsTouched()
        return;
      }
      // this.addEditForm.get('leadStatus')?.setValidators([Validators.required])
      // this.addEditForm.get('leadStatus')?.updateValueAndValidity
      // this.addEditForm.get('Remark')?.setValidators([Validators.required])
      // this.addEditForm.get('Remark')?.updateValueAndValidity

      // if (this.addEditForm.invalid) {
      //   this.addEditForm.markAllAsTouched()
      //   return;
      // }

      const _addEditFormData = this.addEditForm.value;
      console.log(this.addEditForm)


      if (this.queryParamData.type == 'edit') {
        this.manualAssignmentService.updateLeadApproval(this.queryParamData.id, _addEditFormData).subscribe(res => {
          console.log(res, "edit");
          this.toastr.success('Lead Approved Successfully','', { timeOut: 2000 });
          this.router.navigateByUrl('home/manual-assignment', { skipLocationChange: true });
        });
      }
      // else {
      //   this.workListService.createLead(_addEditFormData).subscribe(res => {
      //     console.log(res, "view");
      //   });
      // }

    }
  }

  onSelectFile(event: any) {
    console.log(event, "event")
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        this.fileName.push(event.target.files[i].name);
        this.imgUploadFilename = event.target.files[i].name;
        this.imgUpoladFiletype = event.target.files[i].type;
        var reader = new FileReader();
        reader.onload = (event: any) => {
          console.log(event, " name");
          console.log(event.target.result);
          this.urls.push(event.target.result);
          this.imgUpoloadFilecode = event.target.result
        }

        //                 let fileArrayJsonData={"fileName":event.target.files[i].name,"fileType":event.target.files[i].type,"fileData":this.imgUpoloadFilecode}
        // console.log(fileArrayJsonData);

        //                 this.imagUploadArray.push(fileArrayJsonData)

        reader.readAsDataURL(event.target.files[i]);
        console.log(this.imgUpoloadFilecode, this.urls[i], this.imgUploadFilename, this.imgUpoladFiletype);
        //                this.imagUploadArray
      }
      console.log(this.imgUpoloadFilecode, this.urls, this.imgUploadFilename, this.imgUpoladFiletype);
      console.log(this.imagUploadArray);

    }
    console.log(this.fileName, " this.fileName");
    // const fileSize=this.fileName.length;
    // this.addEditForm.get('file_upload')?.patchValue(fileSize)


  }

  removeFile(id: any) {
    console.log(id);
    this.fileName.splice(id, 1);
    this.urls.splice(id, 1);
    // console.log(this.fileName.length);
    // const fileSize=this.fileName.length;
    // this.addEditForm.get('file_upload')?.patchValue(fileSize)
  }
  addMoreImage() {
    const addMoreImage = this.addEditForm.get('file_upload') as FormArray;
    addMoreImage.push(this.fb.group({
      type: "",
      file: "",
      name: "",
    }))
  }

  //varification
  mobileVerify() {
    this.mobileVerifyBtn = true;
  }
  emailVerify() {
    this.emailVerifyBtn = true;
  }
  //LeadApprovalChange if reject
  LeadApprovalChange(event: any) {
    console.log(event);

    if (event == 'Reject') {
      this.RejectReasonSelect = true;
      this.addEditForm.get('rejectReason')?.setValidators([Validators.required])
      this.addEditForm.get('rejectReason')?.updateValueAndValidity
    }
    else {
      this.RejectReasonSelect = false;
      this.addEditForm.get('rejectReason')?.clearValidators()
      this.addEditForm.get('rejectReason')?.updateValueAndValidity
    }
  }

  checkvalidation(contronname: any, value: any) {
    if (value == "Generated"){
      this.addEditForm.get(contronname)?.setErrors({'incorrect': true})
      this.addEditForm.get(contronname)?.updateValueAndValidity
    }
  }
}
