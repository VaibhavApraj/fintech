import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ManualAssignmentElement } from 'src/app/core/manual-assignment/models/manual-assignment.model';
import { ManualAssignmentService } from 'src/app/core/manual-assignment/service/manual-assignment.service';

@Component({
  selector: 'app-manual-assignment',
  templateUrl: './manual-assignment.component.html',
  styleUrls: ['./manual-assignment.component.css']
})
export class ManualAssignmentComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: ManualAssignmentElement[];
  displayedColumns: string[] = ['leadId','Name', 'mobileNo', 'email', 'dob', 'action'];
  dataSource = new MatTableDataSource<ManualAssignmentElement>(this.ELEMENT_DATA);

  constructor(private router: Router, private manualAssignmentService: ManualAssignmentService) {

  }

  ngOnInit() {

    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/manual-assignment/add-edit-manual-assignment'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.manualAssignmentService.getLeadList().subscribe(res => {
        //console.log(res, "res");
        //delay(1000)
        this.dataSource.data = res as ManualAssignmentElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/manual-assignment/add-edit-manual-assignment', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}
