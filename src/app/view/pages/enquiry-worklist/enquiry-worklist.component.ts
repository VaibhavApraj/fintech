import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EnquiryWorklistElement } from 'src/app/core/enquiry-worklist/models/enquiry-worklist.model';
import { EnquiryWorklistService } from 'src/app/core/enquiry-worklist/service/enquiry-worklist.service';

@Component({
  selector: 'app-enquiry-worklist',
  templateUrl: './enquiry-worklist.component.html',
  styleUrls: ['./enquiry-worklist.component.css']
})
export class EnquiryWorklistComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: EnquiryWorklistElement[];
  displayedColumns: string[] = ['enquiryWorklistId','firstName', 'lastName', 'mobileNo', 'product', 'action'];
  dataSource = new MatTableDataSource<EnquiryWorklistElement>(this.ELEMENT_DATA);

  salesRole:boolean=false;

  spliteRoleName:any;
  makeaRoleArray:any;

  constructor(private router: Router, private enquiryWorklistService: EnquiryWorklistService) {
  }

  ngOnInit() {

    this.spliteRoleName=sessionStorage.getItem('role');
    this.makeaRoleArray= this.spliteRoleName?.split(',');


     for(let j=0;j<this.makeaRoleArray.length;j++)
    {
     console.log(this.makeaRoleArray[j]);
      if(this.makeaRoleArray[j]=='Sales')
      {
        this.salesRole=true;
      }
    }

    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/enquiry-worklist/add-edit-enquiry-worklist'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.enquiryWorklistService.getEnquiryWorklistList().subscribe(res => {
        this.dataSource.data = res as EnquiryWorklistElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/enquiry-worklist/add-edit-enquiry-worklist', { skipLocationChange: true });
  }

  ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

