import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserDesignationService } from '../../../../../core/user-designation/service/userDesignation.service';

@Component({
  selector: 'app-add-edit-user-designation',
  templateUrl: './add-edit-user-designation.component.html',
  styleUrls: ['./add-edit-user-designation.component.css']
})
export class AddEditUserDesignationComponent implements OnInit {

  addEditForm: FormGroup

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';
  _addEditFormData:any;

  constructor(private toastr: ToastrService,private fb: FormBuilder,private router: Router, private userDesignationService: UserDesignationService,private routes:ActivatedRoute) {
    this.addEditForm = this.fb.group({
      id:[''],
      designationCode: ['', Validators.compose([Validators.required])],
      designationName: ['', Validators.compose([Validators.required])],
      active: [true],
    })
  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      this.getSingleData(this.queryParamData.id)
      this.addEditForm.get('designationCode')?.disable()
    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
    }
  }

  getSingleData(id:any){
    this.userDesignationService.getUserDesignationById(id).subscribe(res => {
      console.log(res, "getLeadById");
      //this.addEditForm.patchValue(res);
      this.chekcToggleYesNo(res)
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/user-designation', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.customeTrueFalseName()
      this.userDesignationService.updateUserDesignationById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
               this.toastr.success('User Designation Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/user-designation', { skipLocationChange: true });
      });
    }
    else{
      this.customeTrueFalseName()
     //  this._addEditFormData.active='Yes'
      this.userDesignationService.createUserDesignation(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
               this.toastr.success('User Designation Created Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/user-designation', { skipLocationChange: true });
      });
    }


  }

  customeTrueFalseName()
  {
    this.toggleTrueFalse('active')
  }
  toggleTrueFalse(formvalue: any){
    console.log(4);

    if(this.addEditForm.get(formvalue)?.value == true)
      {
        console.log(5);

        this._addEditFormData[formvalue]='Yes'
      }
      else{
        console.log(6);

        this._addEditFormData[formvalue]='No'
      }
  }
  chekcToggleYesNo(res:any){
    this.putYesNoToTrueFalse(res.active,'active')

    this.addEditForm.get('id')?.patchValue(res.id)
    this.addEditForm.get('designationCode')?.patchValue(res.designationCode)
    this.addEditForm.get('designationName')?.patchValue(res.designationName)
  }
  putYesNoToTrueFalse(resName:any,conrol:any)
  {
    console.log(1);

    if(resName == 'Yes')
    {
      console.log(2);

      this.addEditForm.get(conrol)?.patchValue(true)
    }
    else{
      console.log(3);

      this.addEditForm.get(conrol)?.patchValue(false)
    }
  }
}

