import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { delay } from 'rxjs';
import { ReasonMasterElement } from '../../../../core/reason-master/models/reason-master.model';
import { ReasonMasterService } from '../../../../core/reason-master/service/reason-master.service';

@Component({
  selector: 'app-reason-master',
  templateUrl: './reason-master.component.html',
  styleUrls: ['./reason-master.component.css']
})
export class ReasonMasterComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: ReasonMasterElement[];
  displayedColumns: string[] = ['code', 'description', 'active', 'action'];
  dataSource = new MatTableDataSource<ReasonMasterElement>(this.ELEMENT_DATA);

  constructor(private router: Router, private reasonMasterService: ReasonMasterService) {
  }

  ngOnInit() {
    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/reason-master/add-edit-reason-master'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.reasonMasterService.getReasonMasterList().subscribe(res => {
        //console.log(res, "res");
         //delay(1000)
        this.dataSource.data = res.data as ReasonMasterElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/reason-master/add-edit-reason-master', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

