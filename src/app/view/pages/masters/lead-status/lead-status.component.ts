import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { LeadStatusElement } from 'src/app/core/lead-status/models/leadStatus.model';
import { LeadStatusService } from 'src/app/core/lead-status/service/leadStatus.service';

@Component({
  selector: 'app-lead-status',
  templateUrl: './lead-status.component.html',
  styleUrls: ['./lead-status.component.css']
})
export class LeadStatusComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: LeadStatusElement[];
  displayedColumns: string[] = ['code', 'description', 'active', 'action'];
  dataSource = new MatTableDataSource<LeadStatusElement>(this.ELEMENT_DATA);

  constructor(private router: Router, private leadStatusService: LeadStatusService) {
  }

  ngOnInit() {
      this.getAllDataTable();
  }

  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/lead-status/add-edit-lead-status'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(()=>{
      this.leadStatusService.getLeadStatusList().subscribe(res => {
        this.dataSource.data = res.data as LeadStatusElement[]
      });
  }, 500);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/lead-status/add-edit-lead-status', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

