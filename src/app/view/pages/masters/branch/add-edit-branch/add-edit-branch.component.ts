import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, startWith, map } from 'rxjs';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';
import { PincodeElement } from 'src/app/core/geography-masters/pincode/models/pincode.model';
import { PincodeService } from 'src/app/core/geography-masters/pincode/service/pincode.service';
import { StatesElement } from 'src/app/core/geography-masters/states/models/states.model';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';
import { ReasonMasterElement } from 'src/app/core/reason-master/models/reason-master.model';
import { ReasonMasterService } from 'src/app/core/reason-master/service/reason-master.service';
import { BranchService } from '../../../../../core/branch/service/branch.service';

@Component({
  selector: 'app-add-edit-branch',
  templateUrl: './add-edit-branch.component.html',
  styleUrls: ['./add-edit-branch.component.css']
})
export class AddEditBranchComponent implements OnInit {

  addEditForm: FormGroup
  genderName = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'}
  ];
  countryName:CountriesElement[]=[];
  StateName:StatesElement[]=[];
  CityName:CitiesElement[]=[];
  PincodeName:PincodeElement[]=[];
  branchTypeName = [
    {value: '1', viewValue: 'Area Office'},
    {value: '2', viewValue: 'Head Office'},
    {value: '3', viewValue: 'Regional Office'},
    {value: '4', viewValue: 'State Office'}
  ];
  // regionName = [
  //   {value: '1', viewValue: 'EAST'},
  //   {value: '2', viewValue: 'WEST'},
  //   {value: '3', viewValue: 'NORTH'},
  //   {value: '4', viewValue: 'SOUTH'}
  // ];
  regionName:ReasonMasterElement[]=[];
  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';
  _addEditFormData:any;
  areaValue:any=''

  filterCityName:CitiesElement[]=[];

  searchTextboxControl = new FormControl();
  selectedValues = [];
  filteredOptions: Observable<any[]> | undefined;

  constructor(private toastr: ToastrService,private fb: FormBuilder,private router: Router, private branchService: BranchService,private pincodeService:PincodeService,private routes:ActivatedRoute,private countriesService:CountriesService,private statesService:StatesService,private citiesService:CitiesService,private reasonMasterService:ReasonMasterService) {
    this.addEditForm = this.fb.group({
      id:[''],
      branchCode: ['', Validators.compose([Validators.required])],
      addressLine1: ['', Validators.compose([Validators.required])],
      area: ['', Validators.compose([Validators.required])],
      phoneNo: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern(/^-?(0|[1-9]\d*)?$/),])],
      branchName: ['', Validators.compose([Validators.required])],
      parentBranch: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      // pincode: ['',Validators.compose([Validators.required,Validators.maxLength(6),Validators.minLength(6),Validators.pattern(/^-?(0|[1-9]\d*)?$/),]),],
      postalCode: ['',Validators.compose([Validators.required])],
      externalBranchCode: ['', Validators.compose([Validators.required])],
      active: [true],
      branchType: ['', Validators.compose([Validators.required])],
      region: ['', Validators.compose([Validators.required])],
    })
  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      this.getSingleData(this.queryParamData.id)
      this.addEditForm.get('branchCode')?.disable()
    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
    }
    this.getCountryData();
    this.getStateData();
    this.getCityData();
    this.getPincodeData();
    this.getRegionData();

    this.addEditForm.get('state')?.disable();
    this.addEditForm.get('country')?.disable();
    this.addEditForm.get('postalCode')?.disable();
    this.addEditForm.get('area')?.disable();

    // this.filteredOptions = this.searchTextboxControl.valueChanges
    // .pipe(
    //   startWith<string>(''),
    //   map(name => this._filter(name))
    // );

  }

  // private _filter(name: string): String[] {
  //   const filterValue = name.toLowerCase();
  //   this.setSelectedValues();
  //   this.addEditForm.get('city')?.patchValue(this.selectedValues);
  //   let filteredList = this.CityName.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  //   return filteredList;
  // }


  getCountryData(){
    this.countriesService.getCountriesList().subscribe(res => {
      console.log(res, "country Name");
      this.countryName=res.data;
    });
  }
  getStateData(){
    this.statesService.getStatesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.StateName=res;
    });
  }
  getCityData(){
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "citiesService Name");
      this.CityName=res.data;
      this.filterCityName= this.CityName
    });
  }
  getPincodeData(){
    this.pincodeService.getPincodeList().subscribe(res => {
      console.log(res, "pincodeService Name");
      this.PincodeName=res;
    });
  }
  getRegionData(){
    this.reasonMasterService.getReasonMasterList().subscribe(res => {
      console.log(res, "pincodeService Name");
      this.regionName=res.data;
    });
  }

  //selectyion change

  countrySelect(id:any)
  {
     console.log(id);
    //       const areaValue =this.PincodeName.filter(res=>res.id==id)
    //   console.log("state",areaValue);
    //   this.addEditForm.get('state')?.patchValue(areaValue[0].id)
    //   this.addEditForm.get('state')?.disable();
    this.addEditForm.get('state')?.enable();
    this.countriesService.getStateListByCountryId(id).subscribe(res => {
      console.log(res, "getStateListByCountryId Name");
      this.StateName=res;
    });
  }
  stateSelect(id:any)
  {
    console.log(id);
      //     const areaValue =this.PincodeName.filter(res=>res.id==id)
      // console.log("country",areaValue);
      // this.addEditForm.get('country')?.patchValue(areaValue[0].id)
      // this.addEditForm.get('country')?.updateValueAndValidity()
      // this.addEditForm.get('country')?.disable();
      this.addEditForm.get('city')?.enable();
      this.statesService.getCityListByStateId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.CityName=res;
      });
  }
  citySelect(id:any)
  {
    console.log(id);
      //     const areaValue =this.PincodeName.filter(res=>res.id==id)
      // console.log("state",areaValue);
      // this.addEditForm.get('state')?.patchValue(areaValue[0].id)
      // this.addEditForm.get('state')?.updateValueAndValidity()
      // this.addEditForm.get('state')?.disable();
      this.addEditForm.get('postalCode')?.enable();
      this.citiesService.getPincodeListByCityId(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.PincodeName=res;
      });
      this.citiesService.getAlldetailsByCityId(id).subscribe(res => {
        console.log(res, "getAlldetailsByCityId Name");
       // this.PincodeName=res;
       this.addEditForm.get('country')?.patchValue(res.data.countryId)
       this.addEditForm.get('state')?.patchValue(res.data.stateId)
      });
  }
  pincideSelect(id:any)
  {
      const areaValue =this.PincodeName.filter(res=>res.id==id)
      //this.areaValue=areaValue[0].areaName
      this.addEditForm.get('area')?.patchValue(areaValue[0].areaName)
    //  this.addEditForm.get('area')?.patchValue(this.areaValue)
     // this.addEditForm.get('area')?.disable();
  }

  getSingleData(id:any){
    this.branchService.getBranchById(id).subscribe(res => {
      console.log(res, "getLeadById");
      //this.addEditForm.patchValue(res.data)
      this.chekcToggleYesNo(res.data)
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL Root123
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/branch' , { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.customeTrueFalseName()
      this.branchService.updateBranchById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('Branch Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/branch', { skipLocationChange: true });
      });
    }
    else{
      console.log(this._addEditFormData);

      this.customeTrueFalseName()
     //  this._addEditFormData.active='Yes'
      //this.addEditForm.get('area')?.patchValue(this.areaValue)
      this.branchService.createBranch(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
        this.toastr.success('Branch Created Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/branch', { skipLocationChange: true });
      });
    }


  }

  customeTrueFalseName()
  {
    this.toggleTrueFalse('active')
  }
  toggleTrueFalse(formvalue: any){
    console.log(4);

    if(this.addEditForm.get(formvalue)?.value == true)
      {
        console.log(5);

        this._addEditFormData[formvalue]='Yes'
      }
      else{
        console.log(6);

        this._addEditFormData[formvalue]='No'
      }
  }
  chekcToggleYesNo(res:any){
    console.log(res);

    this.putYesNoToTrueFalse(res.active,'active')
    this.addEditForm.get('id')?.patchValue(res.id)
    this.addEditForm.get('branchCode')?.patchValue(res.branchCode)
    this.addEditForm.get('addressLine1')?.patchValue(res.addressLine1)
    this.addEditForm.get('area')?.patchValue(res.area)
    this.addEditForm.get('phoneNo')?.patchValue(res.phoneNo)
    this.addEditForm.get('branchName')?.patchValue(res.branchName)
    this.addEditForm.get('parentBranch')?.patchValue(res.parentBranch)
    this.addEditForm.get('city')?.patchValue(res.city)
    this.addEditForm.get('country')?.patchValue(res.countr)
    this.addEditForm.get('state')?.patchValue(res.state)
    this.addEditForm.get('postalCode')?.patchValue(res.postalCode)
    this.addEditForm.get('externalBranchCode')?.patchValue(res.externalBranchCode)
    this.addEditForm.get('branchType')?.patchValue(res.branchType)
    this.addEditForm.get('region')?.patchValue(res.region)
  }
  putYesNoToTrueFalse(resName:any,conrol:any)
  {
    console.log(1);

    if(resName == 'Yes')
    {
      console.log(2);

      this.addEditForm.get(conrol)?.patchValue(true)
    }
    else{
      console.log(3);

      this.addEditForm.get(conrol)?.patchValue(false)
    }
  }

    /**
   * Clearing search textbox value
   */
     clearSearch(event:any) {
      event.stopPropagation();
      this.searchTextboxControl.patchValue('');
      this.filterCityName=this.CityName
    }

    /**
     * Set selected values to retain the state
     */
    // setSelectedValues() {
    //   console.log('selectFormControl', this.addEditForm.get('city')?.value);
    //   if (this.addEditForm.get('city')?.value && this.addEditForm.get('city')?.value.length > 0) {
    //     this.addEditForm.get('city')?.value.forEach((e:{}) => {
    //       if (this.selectedValues.indexOf(e:{}) == -1) {
    //         this.selectedValues.push(e:);
    //       }
    //     });
    //   }
    // }

    searchDropdown(searchText:any){
      if(searchText != ''){
        this.filterCityName=this.CityName.filter(Option=>{
          return Option.cityName.toLocaleLowerCase().startsWith(searchText.toLowerCase())
        })
      }else{
        this.filterCityName=this.CityName
      }
      console.log(this.filterCityName);

    }
}
