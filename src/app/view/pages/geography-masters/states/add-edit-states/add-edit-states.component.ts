import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';

@Component({
  selector: 'app-add-edit-states',
  templateUrl: './add-edit-states.component.html',
  styleUrls: ['./add-edit-states.component.css']
})
export class AddEditStatesComponent implements OnInit {

  addEditForm: FormGroup

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';
  countryName:CountriesElement[]=[];
  _addEditFormData:any;
  constructor(private toastr: ToastrService,private fb: FormBuilder,private router: Router, private statesService: StatesService,private routes:ActivatedRoute,private countriesService:CountriesService) {
    this.addEditForm = this.fb.group({
      id: [''],
      country_id: ['', Validators.compose([Validators.required])],
      stateCode: ['', Validators.compose([Validators.required])],
      unionTerritory: [''],
      gstnAvailable: [''],
      stateName: ['', Validators.compose([Validators.required])],
      isDefault: [''],
      gstExempted: [''],
      gstStateCode: ['', Validators.compose([Validators.required])],
      active: [true],
    })
  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      this.getSingleData(this.queryParamData.id)
      this.addEditForm.get('stateCode')?.disable()
    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
    }
    this.getCountryData()
  }
  getCountryData(){
    this.countriesService.getCountriesList().subscribe(res => {
      console.log(res, "country Name");
      this.countryName=res.data;
    });
  }
  getSingleData(id:any){
    this.statesService.getStatesById(id).subscribe(res => {
      console.log(res, "getLeadById");
      //this.addEditForm.patchValue(res)
      this.chekcToggleYesNo(res)
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/states', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
     this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.customeTrueFalseName()
      this.statesService.updateStatesById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('State Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/states', { skipLocationChange: true });
        console.log("this._addEditFormData",this._addEditFormData);
      });
    }
    else{
      this.customeTrueFalseName()
     //  this._addEditFormData.active='Yes'
      this.statesService.createStates(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
        this.toastr.success('State Created Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/states', { skipLocationChange: true });
        console.log("this._addEditFormData",this._addEditFormData);

      });
    }
  }

  customeTrueFalseName()
  {
    this.toggleTrueFalse('active')
    this.toggleTrueFalse('unionTerritory')
    this.toggleTrueFalse('gstnAvailable')
    this.toggleTrueFalse('isDefault')
    this.toggleTrueFalse('gstExempted')
  }
  toggleTrueFalse(formvalue: any){
    console.log(4);

    if(this.addEditForm.get(formvalue)?.value == true)
      {
        console.log(5);

        this._addEditFormData[formvalue]='Yes'
      }
      else{
        console.log(6);

        this._addEditFormData[formvalue]='No'
      }
  }
  chekcToggleYesNo(res:any){
    this.putYesNoToTrueFalse(res.active,'active')
    this.putYesNoToTrueFalse(res.isDefault,'isDefault')
    this.putYesNoToTrueFalse(res.unionTerritory,'unionTerritory')
    this.putYesNoToTrueFalse(res.gstnAvailable,'gstnAvailable')
    this.putYesNoToTrueFalse(res.gstExempted,'gstExempted')

    this.addEditForm.get('id')?.patchValue(res.id)
    this.addEditForm.get('stateCode')?.patchValue(res.stateCode)
    this.addEditForm.get('stateName')?.patchValue(res.stateName)
    this.addEditForm.get('country_id')?.patchValue(res.country.id)
    this.addEditForm.get('gstStateCode')?.patchValue(res.gstStateCode)
  }
  putYesNoToTrueFalse(resName:any,conrol:any)
  {
    console.log(1);

    if(resName == 'Yes')
    {
      console.log(2);

      this.addEditForm.get(conrol)?.patchValue(true)
    }
    else{
      console.log(3);

      this.addEditForm.get(conrol)?.patchValue(false)
    }
  }
}


