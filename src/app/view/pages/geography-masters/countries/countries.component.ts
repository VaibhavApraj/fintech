import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { delay } from 'rxjs';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: CountriesElement[];
  displayedColumns: string[] = ['code', 'description', 'active', 'action'];
  dataSource = new MatTableDataSource<CountriesElement>(this.ELEMENT_DATA);

  constructor(private router: Router, private countriesService: CountriesService,private changeDetectorRefs: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/countries/add-edit-countries'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.countriesService.getCountriesList().subscribe(res => {
        //console.log(res, "res");
        //delay(1000)
        this.dataSource.data = res.data as CountriesElement[]
        //this.changeDetectorRefs.detectChanges();
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/countries/add-edit-countries', { skipLocationChange: true });
  }

  ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

