import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { StatesElement } from 'src/app/core/geography-masters/states/models/states.model';
import { StatesService } from 'src/app/core/geography-masters/states/service/states.service';
import { CountriesService } from 'src/app/core/geography-masters/countries/service/countries.service';
import { CountriesElement } from 'src/app/core/geography-masters/countries/models/countries.model';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-edit-cities',
  templateUrl: './add-edit-cities.component.html',
  styleUrls: ['./add-edit-cities.component.css']
})
export class AddEditCitiesComponent implements OnInit {

  addEditForm: FormGroup

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';

  // countryName = [
  //   {value: '1', viewValue: 'India'},
  //   {value: '2', viewValue: 'USA'},
  //   {value: '3', viewValue: 'UK'},
  //   {value: '4', viewValue: 'Itali'}
  // ];
  // StateName = [
  //   {value: '1', viewValue: 'Pune'},
  //   {value: '2', viewValue: 'Maharastra'},
  //   {value: '3', viewValue: 'Nashi'},
  //   {value: '4', viewValue: 'Sangli'}
  // ];

  countryName:CountriesElement[]=[];
  StateName:StatesElement[]=[];
  cityClassificationName = [
    {value: '1', viewValue: 'Tier1'},
    {value: '2', viewValue: 'Tier2'},
    {value: '3', viewValue: 'Tier3'},
    {value: '4', viewValue: 'Tier4'}
  ];
  _addEditFormData:any;

  constructor(private toastr: ToastrService,private fb: FormBuilder,private router: Router, private citiesService: CitiesService,private routes:ActivatedRoute,private statesService:StatesService,private countriesService:CountriesService) {
    this.addEditForm = this.fb.group({
      id: [''],
      countryId: ['', Validators.compose([Validators.required])],
      stateId: ['', Validators.compose([Validators.required])],
      cityCode: ['', Validators.compose([Validators.required])],
      cityName: ['', Validators.compose([Validators.required])],
      cityClassification: ['', Validators.compose([Validators.required])],
      active: [true],
    })
  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      this.addEditForm.get('countryId')?.disable()
      this.addEditForm.get('cityCode')?.disable()
      this.getSingleData(this.queryParamData.id)
    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
      this.addEditForm.get('countryId')?.disable()
    }
    this.getStateData();
    this.getCountryData();
    this.addEditForm.get('state')?.disable();
  }

  getStateData(){
    this.statesService.getStatesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.StateName=res;
    });
  }
  getCountryData(){
    this.countriesService.getCountriesList().subscribe(res => {
      console.log(res, "country Name");
      this.countryName=res.data;
    });
  }
  getSingleData(id:any){
    this.citiesService.getCitiesById(id).subscribe(res => {
      console.log(res, "getLeadById");
      // this.addEditForm.patchValue(res)
      this.chekcToggleYesNo(res.data)
    });
  }
  countrySelect(id:any)
  {
     console.log(id);
    this.addEditForm.get('state')?.enable();
    this.countriesService.getStateListByCountryId(id).subscribe(res => {
      console.log(res, "getStateListByCountryId Name");
      this.StateName=res;
    });
  }
    stateSelect(id:any)
  {
    console.log(id);
      //this.addEditForm.get('city')?.enable();
      this.statesService.getStatesById(id).subscribe(res => {
        console.log(res, "getCityListByStateId Name");
        this.addEditForm.get('countryId')?.patchValue(res.country.countryId)
      });
      // this.satetPlaceHolder='Select State'
      // this.pincodePlaceHolder='Select Pincode'
      // this.AsssinedToPlaceHolder='Select Asssined To'
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/cities', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.customeTrueFalseName()
      this.citiesService.updateCitiesById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('City Updated Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/cities', { skipLocationChange: true });
      });
    }
    else{
      this.customeTrueFalseName()
     //  this._addEditFormData.active='Yes'
      this.citiesService.createCities(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
        this.toastr.success('City Created Successfully','', { timeOut: 2000 });
        this.router.navigateByUrl('home/cities', { skipLocationChange: true });
      });
    }


  }

  customeTrueFalseName()
  {
    this.toggleTrueFalse('active')
  }
  toggleTrueFalse(formvalue: any){
    console.log(4);

    if(this.addEditForm.get(formvalue)?.value == true)
      {
        console.log(5);

        this._addEditFormData[formvalue]='Yes'
      }
      else{
        console.log(6);

        this._addEditFormData[formvalue]='No'
      }
  }
  chekcToggleYesNo(res:any){
    this.putYesNoToTrueFalse(res.active,'active')

    this.addEditForm.get('id')?.patchValue(res.id)
    this.addEditForm.get('countryId')?.patchValue(res.countryId)
    this.addEditForm.get('stateId')?.patchValue(res.state.id)
    this.addEditForm.get('cityCode')?.patchValue(res.cityCode)
    this.addEditForm.get('cityName')?.patchValue(res.cityName)
    this.addEditForm.get('cityClassification')?.patchValue(res.cityClassification)
  }
  putYesNoToTrueFalse(resName:any,conrol:any)
  {
    console.log(1);

    if(resName == 'Yes')
    {
      console.log(2);

      this.addEditForm.get(conrol)?.patchValue(true)
    }
    else{
      console.log(3);

      this.addEditForm.get(conrol)?.patchValue(false)
    }
  }
}

