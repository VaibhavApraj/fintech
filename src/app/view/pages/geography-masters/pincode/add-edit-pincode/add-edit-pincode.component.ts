import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CitiesElement } from 'src/app/core/geography-masters/cities/models/cities.model';
import { CitiesService } from 'src/app/core/geography-masters/cities/service/cities.service';
import { PincodeElement } from 'src/app/core/geography-masters/pincode/models/pincode.model';
import { PincodeService } from 'src/app/core/geography-masters/pincode/service/pincode.service';

@Component({
  selector: 'app-add-edit-pincode',
  templateUrl: './add-edit-pincode.component.html',
  styleUrls: ['./add-edit-pincode.component.css']
})
export class AddEditPincodeComponent implements OnInit {

  addEditForm: FormGroup

  queryParamData:any;
  saveBtn:boolean=true;
  createBtn:boolean=true;
  addEditHeadTitle:any;
  createAddEditBtnName='';

  // cityName = [
  //   {value: '1', viewValue: 'Mumbai'},
  //   {value: '2', viewValue: 'Pune'},
  //   {value: '3', viewValue: 'Nashik'},
  //   {value: '4', viewValue: 'Beed'}
  // ];
  CityName:CitiesElement[]=[];
  _addEditFormData:any;

  constructor(private toastr: ToastrService,private fb: FormBuilder,private router: Router, private pincodeService: PincodeService,private routes:ActivatedRoute,private citiesService:CitiesService) {
    this.addEditForm = this.fb.group({
      id:[''],
      pincode: ['', Validators.compose([Validators.required])],
      city_id: ['', Validators.compose([Validators.required])],
      areaName: ['', Validators.compose([Validators.required])],
      serviceable: [''],
      active: [true],
    })
  }

  ngOnInit(): void {
    this.routes.queryParams.subscribe(res=>this.queryParamData=res);
    if(this.queryParamData.type=='edit')
    {
      this.saveBtn=true;
      this.createBtn=true;
      this.addEditHeadTitle='Edit'
      this.createAddEditBtnName='Submit'
      // this.addEditForm.get('pincode')?.disable()
      this.getSingleData(this.queryParamData.id)
    }else if(this.queryParamData.type=='view'){
      this.addEditHeadTitle='View'
      this.saveBtn=false;
      this.createBtn=false;
      this.getSingleData(this.queryParamData.id);
      this.addEditForm.disable();
    }
    else{
      this.addEditHeadTitle='Create'
      this.createAddEditBtnName='Create'
    }
    this.getCityData();
  }
  getCityData(){
    this.citiesService.getCitiesList().subscribe(res => {
      console.log(res, "StateName Name");
      this.CityName=res.data;
    });
  }
  getSingleData(id:any){
    this.pincodeService.getPincodeById(id).subscribe(res => {
      console.log(res, "getLeadById");
      // this.addEditForm.patchValue(res)
      this.chekcToggleYesNo(res)
    });
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.addEditForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  omitCharacters(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  omitSpecialChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      k == 32 ||
      (k >= 48 && k <= 57)
    );
  }

  acceptChar(event: any) {
    let k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (
      (k > 64 && k < 91) ||
      (k > 96 && k < 123) ||
      k == 8 ||
      (k >= 48 && k <= 57) ||
      k == 47
      || k == 32       //accept forward slash for DL
    );
  }

  cancelAddEditForm(){
    this.router.navigateByUrl('home/pincode', { skipLocationChange: true });
  }
  saveAddEditForm(){

  }
  createAddEditForm(){
    if (this.addEditForm.invalid) {
      this.addEditForm.markAllAsTouched()
      return;
    }
    this._addEditFormData = this.addEditForm.value;
    console.log(this.addEditForm)

    if(this.queryParamData.type=='edit'){
      this.customeTrueFalseName()
      this.pincodeService.updatePincodeById(this.queryParamData.id,this._addEditFormData).subscribe(res => {
        console.log(res, "edit");
        this.toastr.success('Pincode Updated Successfully','', { timeOut: 2000 });
      });
    }
    else{
      this.customeTrueFalseName()
     //  this._addEditFormData.active='Yes'
      this.pincodeService.createPincode(this._addEditFormData).subscribe(res => {
        console.log(res, "res");
        this.toastr.success('Pincode Created Successfully','', { timeOut: 2000 });
      });
    }
    this.router.navigateByUrl('home/pincode', { skipLocationChange: true });

  }

  customeTrueFalseName()
  {
    this.toggleTrueFalse('active')
    this.toggleTrueFalse('serviceable')
  }
  toggleTrueFalse(formvalue: any){
    console.log(4);

    if(this.addEditForm.get(formvalue)?.value == true)
      {
        console.log(5);

        this._addEditFormData[formvalue]='Yes'
      }
      else{
        console.log(6);

        this._addEditFormData[formvalue]='No'
      }
  }
  chekcToggleYesNo(res:any){
    this.putYesNoToTrueFalse(res.active,'active')
    this.putYesNoToTrueFalse(res.serviceable,'serviceable')

    this.addEditForm.get('id')?.patchValue(res.id)
    //this.addEditForm.get('country_id')?.patchValue(res.country_id)
    this.addEditForm.get('city_id')?.patchValue(res.city.id)
    this.addEditForm.get('pincode')?.patchValue(res.pincode)
    this.addEditForm.get('areaName')?.patchValue(res.areaName)
    // this.addEditForm.get('cityClassification')?.patchValue(res.cityClassification)
  }
  putYesNoToTrueFalse(resName:any,conrol:any)
  {
    console.log(1);

    if(resName == 'Yes')
    {
      console.log(2);

      this.addEditForm.get(conrol)?.patchValue(true)
    }
    else{
      console.log(3);

      this.addEditForm.get(conrol)?.patchValue(false)
    }
  }
}

