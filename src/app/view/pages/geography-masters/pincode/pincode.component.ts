import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { delay } from 'rxjs';
import { PincodeElement } from 'src/app/core/geography-masters/pincode/models/pincode.model';
import { PincodeService } from 'src/app/core/geography-masters/pincode/service/pincode.service';

@Component({
  selector: 'app-pincode',
  templateUrl: './pincode.component.html',
  styleUrls: ['./pincode.component.css']
})
export class PincodeComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: PincodeElement[];
  displayedColumns: string[] = ['pincode', 'city', 'area','active', 'action'];
  dataSource = new MatTableDataSource<PincodeElement>(this.ELEMENT_DATA);

  constructor(private router: Router, private pincodeService: PincodeService) {
  }

  ngOnInit() {

    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/pincode/add-edit-pincode'], navigationExtras);
  }
  getAllDataTable() {
    setTimeout(() => {
      this.pincodeService.getPincodeList().subscribe(res => {
        //console.log(res, "res");
        //delay(1000)
        this.dataSource.data = res as PincodeElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/pincode/add-edit-pincode', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}

