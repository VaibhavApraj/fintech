import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UserElement } from '../../../core/user/models/user.model';
import { UserService } from '../../../core/user/service/user.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  ELEMENT_DATA!: UserElement[];
  displayedColumns: string[] = [ 'loginID','firstName', 'lastName','userType','userName', 'mobileNo', 'action'];
  dataSource = new MatTableDataSource<UserElement>(this.ELEMENT_DATA);

  animal: string | undefined;
  name: string | undefined;

  autoFocus?: boolean = true;
  constructor(public dialog: MatDialog,private router: Router, private userService: UserService) {
  }

  ngOnInit() {

    this.getAllDataTable();
  }
  editViewAction(id: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
    this.router.navigate(['/home/user/add-edit-user'], navigationExtras);
  }
  changePasswordAction(id: any, fname: any, lname: any, type: any) {
    let navigationExtras = {
      queryParams: { 'id': id,'type':type },
      fragment: 'anchor',
      skipLocationChange: true
    };
   // this.router.navigate(['/home/user/reset-password'], navigationExtras);

    const dialogRef = this.dialog.open(ResetPasswordComponent, {
      width: '500px',
      data: {id: id, fname: fname,lname: lname, type: type},
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  getAllDataTable() {
    setTimeout(() => {
      this.userService.getUserList().subscribe(res => {
        console.log(res, "res");
        //delay(1000)
        this.dataSource.data = res as UserElement[]
      });
    }, 500);

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addData() {
    this.router.navigateByUrl('home/user/add-edit-user', { skipLocationChange: true });
  }

ngOnDestroy(){
    this.dataSource.disconnect()
  }
}


