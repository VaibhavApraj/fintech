import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BranchElement } from '../models/branch.model';

@Injectable({
  providedIn: 'root',
})
export class BranchService {

  constructor(private httpClient: HttpClient) {}

  getBranchList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllBranch`);
  }

  createBranch(branch:any): Observable<any> {
    console.log("branch",branch)
     return this.httpClient.post<any>(`api/v1/createBranch`, branch);
  }

  getBranchById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getBranchById/`+id);
  }

  updateBranchById(id: Number, branch: BranchElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateBranch/`+id, branch);
  }

}
