import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserElement } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseUrl = 'http://192.168.2.228:8080/api/v1/user';

  constructor(private httpClient: HttpClient) {}

  getUserList(): Observable<UserElement[]> {
    return this.httpClient.get<UserElement[]>(`api/v1/user`);
  }

  createUser(User:any): Observable<UserElement[]> {
    console.log("User",User)
     return this.httpClient.post<UserElement[]>(`api/v1/user`, User);
  }

  getUserById(id: Number): Observable<UserElement> {
    return this.httpClient.get<UserElement>(`api/v1/user/`+id);
  }

  updateUserById(id: Number, User: UserElement): Observable<object> {
  //   const config ={

  //       "id": User.id,
  //       "firstName": User.firstName,
  //       "lastName": User.lastName,
  //       "userType": User.userType,
  //       "designation": User.designation,
  //       "reportingManager": User.reportingManager,
  //       "lock": User.lock,
  //       "gender": User.gender,
  //       "level": User.level,
  //       "salutation": User.salutation,
  //       "email": User.email,
  //       "mobileNo": User.mobileNo

  // }
    return this.httpClient.put(`api/v1/user/`+id, User);
  }

  deleteUser(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }

  getLoginDetails(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getLoginDetails/`+id);
  }

  updateUserPasswordById(id: Number, User:any): Observable<any> {
    const config ={
        "id": User.id,
        "password": User.newPassword
  }
    return this.httpClient.put(`api/v1/updateUserPassword/`+id, User);
  }

  updateUserProfileById(id: Number, User: UserElement): Observable<any> {
    return this.httpClient.put(`api/v1/updateUserProfile/`+id, User);
  }
}
