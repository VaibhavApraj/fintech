export class EnquiryElement {
    id!: number;
    createdTime:any;
    lastModifiedTime!:any;

    firstName!: string;
    lastName!: string;
    mobileNo!: string;
    postalCode!: string;
    city!: string;
    product!: string;
  }
