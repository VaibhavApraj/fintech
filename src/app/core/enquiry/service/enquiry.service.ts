import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnquiryElement } from '../models/enquiry.model';

@Injectable({
  providedIn: 'root',
})
export class EnquiryService {

  constructor(private httpClient: HttpClient) {}

  getEnquiryList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/enquiry`);
  }

  createEnquiry(enquiry:any): Observable<any> {
    console.log("enquiry",enquiry)
     return this.httpClient.post<any>(`api/v1/enquiry`, enquiry);
  }

  getEnquiryById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/enquiry/`+id);
  }

  updateEnquiryById(id: Number, enquiry: EnquiryElement): Observable<object> {
    return this.httpClient.put(`api/v1/enquiry/`+id, enquiry);
  }

  autoAssignUserByRole(id:any,role:any): Observable<any> {
    const config ={
        "userId": id,
        "userRole": role,
    }
    console.log("enquiry",config)
     return this.httpClient.post<any>(`api/v1/autoAssignUserByRole`, config);
  }
}
