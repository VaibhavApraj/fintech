import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WorkListElement } from '../models/work-list.model';


@Injectable({
  providedIn: 'root',
})
export class WorkListService {

  constructor(private httpClient: HttpClient) {}

  // getLeadList(): Observable<WorkListElement[]> {
  //   return this.httpClient.get<WorkListElement[]>(`api/v1/lead`);
  // }

  getWorkList(id:any): Observable<WorkListElement[]> {
    return this.httpClient.get<WorkListElement[]>(`api/v1/getWorklist/`+id);
  }

  createLead(lead:any): Observable<WorkListElement[]> {
     return this.httpClient.post<WorkListElement[]>(`api/v1/lead`, lead);
  }

  getLeadById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/lead/`+id);
  }

  updateLeadById(id: Number, lead: WorkListElement): Observable<object> {
    return this.httpClient.put(`api/v1/lead/`+id, lead);
  }

  updateWorklist(id: Number, lead: any): Observable<object> {
    const config ={
      "id": lead.id,
      "loanAmount": lead.loanAmount,
      "product": lead.product,
      "tenor": lead.tenor,
      "remarkLeadLoan": lead.remarkLeadLoan,
      "file_upload": lead.file_upload,
      "leadStatusId":lead.leadLoanStatus

      // "mobileNoVarification": lead.mobileNoVarification,
      // "emailIdVarification": lead.emailIdVarification,
  }
    return this.httpClient.put(`api/v1/updateWorklist/`+id, config);//
  }

  // getLeadApproval(id:any): Observable<WorkListElement[]> {
  //   const config ={
  //     "id": id,
  // }
  //   return this.httpClient.put<WorkListElement[]>(`api/v1/leadApproval/`+id,config);
  // }

  updateLeadmaintance(id: Number, lead:any): Observable<object> {
    const config ={
      "id": lead.id,
      "scheduleDate": lead.scheduleDate,
      "convenientTime": lead.convenientTime,
      "scheduledRemark": lead.scheduledRemark,
      "leadStatusId":lead.leadScheduleStatus
  }
  console.log("lead",config)
    return this.httpClient.put(`api/v1/updateLeadMaintance/`+id, config);
  }

  updateLeadApproval(id: Number, lead: WorkListElement): Observable<object> {
    const config ={
      "id": lead.id,
      "leadStage": lead.leadStage,
      "rejectReason": lead.rejectReason,
      "approvedRemark": lead.approvedRemark,
     // "leadStatusId":lead.leadApprovedStatus
  }
  console.log("lead",config)
    return this.httpClient.put(`api/v1/leadApproval/`+id, config);
  }

  mobileNoVarification(id:any,lead:any): Observable<any> {
    const config ={
      "mobileNumber": lead,
  }
    return this.httpClient.post<any>(`api/v1/sendOtpToMobile/`+id, config);
 }

 mobileNoOTPVarification(id:any,lead:any): Observable<any> {
  const config ={
    "otp": lead,
}
  return this.httpClient.post<any>(`api/v1/verifyOtpToMobile/`+id, config);
}

 emailIdVarification(id:any,lead:any): Observable<any> {
  const config ={
    "emailNumber": lead,
}
  return this.httpClient.post<any>(`api/v1/sendOtpToEmail/`+id, config);
}

emailIOTPVarification(id:any,lead:any): Observable<any> {
  const config ={
    "otp": lead,
}
  return this.httpClient.post<any>(`api/v1/verifyOtpToEmail/`+id, config);
}
}
