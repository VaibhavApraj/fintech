import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StatesElement } from '../models/states.model';

@Injectable({
  providedIn: 'root',
})
export class StatesService {

  constructor(private httpClient: HttpClient) {}

  getStatesList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllState`);
  }

  createStates(states:any): Observable<any> {
    console.log("states",states)
     return this.httpClient.post<any>(`api/v1/addState`, states);
  }

  getStatesById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getStateById/`+id);
  }

  updateStatesById(id: Number, states: StatesElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateState/`+id, states);
  }

  getCityListByStateId(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getCityByStateId/`+id);
  }

}


