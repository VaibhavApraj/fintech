export class StatesElement {
  id!: number;
  createdTime:any;
  lastModifiedTime!:any;
  country!:any;
  country_id!: string;
  stateCode!: string;
  stateName!: string;
  gstStateCode!:any;

  unionTerritory!: string;
  gstnAvailable!: string;
  gstExempted:any;

  active!: string;
  data!:string;
  //provinceState!: string;
  isDefault!: number;
  }
