import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AreasElement } from '../models/areas.model';

@Injectable({
  providedIn: 'root',
})
export class AreasService {
  private baseUrl = 'http://192.168.2.228:8080/api/v1/areas';

  constructor(private httpClient: HttpClient) {}

  getAreasList(): Observable<AreasElement[]> {
    return this.httpClient.get<AreasElement[]>(`api/v1/areas`);
  }

  createAreas(areas:any): Observable<AreasElement[]> {
    console.log("areas",areas)
     return this.httpClient.post<AreasElement[]>(`api/v1/areas`, areas);
  }

  getAreasById(id: Number): Observable<AreasElement> {
    return this.httpClient.get<AreasElement>(`api/v1/areas/`+id);
  }

  updateAreasById(id: Number, areas: AreasElement): Observable<object> {
    return this.httpClient.put(`api/v1/areas/`+id, areas);
  }

  deleteAreas(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}
