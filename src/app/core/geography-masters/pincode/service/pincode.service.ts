import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PincodeElement } from '../models/pincode.model';

@Injectable({
  providedIn: 'root',
})
export class PincodeService {

  constructor(private httpClient: HttpClient) {}

  getPincodeList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllPincode`);
  }

  createPincode(pincode:any): Observable<any> {
    console.log("pincode",pincode)
     return this.httpClient.post<any>(`api/v1/addPincode`, pincode);
  }

  getPincodeById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getPincodeById/`+id);
  }

  updatePincodeById(id: Number, pincode: PincodeElement): Observable<object> {
    return this.httpClient.put(`api/v1/updatePincode/`+id, pincode);
  }

  getUserByPincodeId(id: Number): Observable<object> {
    return this.httpClient.get(`api/v1/getUserByPincodeId/`+id);
  }
}
