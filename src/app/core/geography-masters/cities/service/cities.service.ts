import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CitiesElement } from '../models/cities.model';

@Injectable({
  providedIn: 'root',
})
export class CitiesService {
  constructor(private httpClient: HttpClient) {}

  getCitiesList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllCity`);
  }

  createCities(cities:any): Observable<any> {
    console.log("cities",cities)
     return this.httpClient.post<any>(`api/v1/addCity`, cities);
  }

  getCitiesById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getCityById/`+id);
  }

  updateCitiesById(id: Number, cities: CitiesElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateCity/`+id, cities);
  }

  getPincodeListByCityId(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getPincodeByCityId/`+id);
  }

  getAlldetailsByCityId(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAlldetailsByCityId/`+id);
  }
}
