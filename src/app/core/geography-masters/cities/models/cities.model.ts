export class CitiesElement {
  id!: number;
  createdTime:any;
  lastModifiedTime!:any;

  countryId!: string;
  province!: string;


  stateId!: string;
  cityCode!: string;
  cityName!: string;
  cityClassification!: number;
  active!: string;
  }
