import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CountriesElement } from '../models/countries.model';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {
  private baseUrl = 'http://192.168.2.228:8080/api/v1/countries';

  constructor(private httpClient: HttpClient) {}

  getCountriesList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllCountry`);
  }

  createCountries(countries:any): Observable<any> {
    console.log("countries",countries)
     return this.httpClient.post<any>(`api/v1/addCountry`, countries);
  }

  getCountriesById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getCountryById/`+id);
  }

  updateCountriesById(id: Number, countries: CountriesElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateCountry/`+id, countries);
  }

  getStateListByCountryId(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getStateByCountryId/`+id);
  }

}
