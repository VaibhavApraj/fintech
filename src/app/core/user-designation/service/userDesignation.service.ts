import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDesignationElement } from '../models/userDesignation.model';

@Injectable({
  providedIn: 'root',
})
export class UserDesignationService {
  private baseUrl = 'http://192.168.2.228:8080/api/v1/userDesignation';

  constructor(private httpClient: HttpClient) {}

  getUserDesignationList(): Observable<UserDesignationElement[]> {
    return this.httpClient.get<UserDesignationElement[]>(`api/v1/getAllDesignation`);
  }

  createUserDesignation(userDesignation:any): Observable<UserDesignationElement[]> {
    console.log("userDesignation",userDesignation)
     return this.httpClient.post<UserDesignationElement[]>(`api/v1/addDesignation`, userDesignation);
  }

  getUserDesignationById(id: Number): Observable<UserDesignationElement> {
    return this.httpClient.get<UserDesignationElement>(`api/v1/getDesignationById/`+id);
  }

  updateUserDesignationById(id: Number, userDesignation: UserDesignationElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateDesignation/`+id, userDesignation);
  }
}
