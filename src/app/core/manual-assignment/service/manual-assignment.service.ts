import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ManualAssignmentElement } from '../models/manual-assignment.model';


@Injectable({
  providedIn: 'root',
})
export class ManualAssignmentService {

  constructor(private httpClient: HttpClient) {}

  getLeadList(): Observable<ManualAssignmentElement[]> {
    return this.httpClient.get<ManualAssignmentElement[]>(`api/v1/lead`);
  }

  createLead(lead:any): Observable<ManualAssignmentElement[]> {
     return this.httpClient.post<ManualAssignmentElement[]>(`api/v1/lead`, lead);
  }

  getLeadById(id: Number): Observable<ManualAssignmentElement> {
    return this.httpClient.get<ManualAssignmentElement>(`api/v1/lead/`+id);
  }

  updateLeadById(id: Number, lead: ManualAssignmentElement): Observable<object> {
    return this.httpClient.put(`api/v1/lead/`+id, lead);
  }

  updateWorklist(id: Number, lead: ManualAssignmentElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateWorklist/`+id, lead);
  }

  updateLeadApproval(id: Number, lead: ManualAssignmentElement): Observable<object> {
    const config ={
      "id": lead.id,
      "leadStatus": lead.leadStatus,
      "rejectReason": lead.rejectReason,
      "approvedRemark": lead.approvedRemark,
  }
  console.log("lead",config)
    return this.httpClient.put(`api/v1/leadApproval/`+id, config);
  }
}
