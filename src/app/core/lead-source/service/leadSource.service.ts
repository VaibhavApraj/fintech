import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { LeadSourceElement } from '../models/leadSource.model';

@Injectable({
  providedIn: 'root',
})
export class LeadSourceService {

  constructor(private httpClient: HttpClient) {}

  getLeadSourceList(): Observable<any> {
    return this.httpClient.get(`api/v1/getAllLeadsource`);
  }

  createLeadSource(leadSource:any): Observable<any> {
    console.log("leadSource",leadSource)
     return this.httpClient.post<any>(`api/v1/createLeadsource`, leadSource);
  }

  getLeadSourceById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getLeadsourceById/`+id);
  }

  updateLeadSourceById(id: Number, leadSource: LeadSourceElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateLeadsource/`+id, leadSource);
  }
}
