import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RoleElement } from '../models/role.model';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  private baseUrl = 'http://192.168.2.228:8080/api/v1/role';

  constructor(private httpClient: HttpClient) {}

  getRoleList(): Observable<RoleElement[]> {
    return this.httpClient.get<RoleElement[]>(`api/v1/role`);
  }

  createRole(role:any): Observable<RoleElement[]> {
    console.log("role",role)
     return this.httpClient.post<RoleElement[]>(`api/v1/role`, role);
  }

  getRoleById(id: Number): Observable<RoleElement> {
    return this.httpClient.get<RoleElement>(`api/v1/role/`+id);
  }

  updateRoleById(id: Number, role: RoleElement): Observable<object> {
    return this.httpClient.put(`api/v1/role/`+id, role);
  }

  deleteRole(id: Number): Observable<object> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }
}
