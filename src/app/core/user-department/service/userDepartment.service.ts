import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDepartmentElement } from '../models/userDepartment.model';

@Injectable({
  providedIn: 'root',
})
export class UserDepartmentService {

  constructor(private httpClient: HttpClient) {}

  getUserDepartmentList(): Observable<UserDepartmentElement[]> {
    return this.httpClient.get<UserDepartmentElement[]>(`api/v1/getAllDepartment`);
  }

  createUserDepartment(userDepartment:any): Observable<UserDepartmentElement[]> {
    console.log("userDepartment",userDepartment)
     return this.httpClient.post<UserDepartmentElement[]>(`api/v1/createDepartment`, userDepartment);
  }

  getUserDepartmentById(id: Number): Observable<UserDepartmentElement> {
    return this.httpClient.get<UserDepartmentElement>(`api/v1/getDepartementById/`+id);
  }

  updateUserDepartmentById(id: Number, userDepartment: UserDepartmentElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateDepartment/`+id, userDepartment);
  }

}

