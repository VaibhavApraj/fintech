import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { LeadStatusElement } from '../models/leadStatus.model';

@Injectable({
  providedIn: 'root',
})
export class LeadStatusService {

  constructor(private httpClient: HttpClient) {}

  getLeadStatusList(): Observable<any> {
    return this.httpClient.get(`api/v1/getAllLeadStatus`);
  }

  createLeadStatus(leadStatus:any): Observable<any> {
    console.log("leadStatus",leadStatus)
     return this.httpClient.post<any>(`api/v1/createLeadStatus`, leadStatus);
  }

  getLeadStatusById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getLeadStatusById/`+id);
  }

  updateLeadStatusById(id: Number, leadStatus: LeadStatusElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateLeadStatus/`+id, leadStatus);
  }
}
