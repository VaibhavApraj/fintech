import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { ReasonMasterElement } from '../models/reason-master.model';

@Injectable({
  providedIn: 'root',
})
export class ReasonMasterService {

  constructor(private httpClient: HttpClient) {}

  getReasonMasterList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllRegion`);
  }

  createReasonMaster(reasonMaster:any): Observable<any> {
    console.log("reasonMaster",reasonMaster)
     return this.httpClient.post<AnyCatcher>(`api/v1/createRegion`, reasonMaster);
  }

  getReasonMasterById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getRegionById/`+id);
  }

  updateReasonMasterById(id: Number, reasonMaster: ReasonMasterElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateRegion/`+id, reasonMaster);
  }

}
