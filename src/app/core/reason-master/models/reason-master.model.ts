export class ReasonMasterElement {
    id!: number;
    createdTime:any;
    lastModifiedTime!:any;

    regionCode!: string;
    regionName!: string;
    active!: string;
    data!:string;
  }
