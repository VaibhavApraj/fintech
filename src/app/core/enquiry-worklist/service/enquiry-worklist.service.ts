import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnquiryWorklistElement } from '../models/enquiry-worklist.model';

@Injectable({
  providedIn: 'root',
})
export class EnquiryWorklistService {

  constructor(private httpClient: HttpClient) {}

  getEnquiryWorklistList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/enquiryWorklist`);
  }

  createEnquiryWorklist(enquiryWorklist:any): Observable<any> {
    console.log("enquiryWorklist",enquiryWorklist)
     return this.httpClient.post<any>(`api/v1/enquiryWorklist`, enquiryWorklist);
  }

  getEnquiryWorklistById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/enquiryWorklist/`+id);
  }

  updateEnquiryWorklistById(id: Number, enquiryWorklist: EnquiryWorklistElement): Observable<object> {
    return this.httpClient.put(`api/v1/enquiryWorklist/`+id, enquiryWorklist);
  }

  autoAssignUserByRole(id:any,role:any): Observable<any> {
    const config ={
        "userId": id,
        "userRole": role,
    }
    console.log("enquiryWorklist",config)
     return this.httpClient.post<any>(`api/v1/autoAssignUserByRole`, config);
  }
}
