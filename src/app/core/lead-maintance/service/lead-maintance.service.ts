import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LeadMaintanceElement } from '../models/lead-maintance.model';


@Injectable({
  providedIn: 'root',
})
export class LeadMaintanceService {

  constructor(private httpClient: HttpClient) {}

  getLeadList(): Observable<LeadMaintanceElement[]> {
    return this.httpClient.get<LeadMaintanceElement[]>(`api/v1/lead`);
  }

  createLead(lead:any): Observable<LeadMaintanceElement[]> {
     return this.httpClient.post<LeadMaintanceElement[]>(`api/v1/lead`, lead);
  }

  getLeadById(id: Number): Observable<LeadMaintanceElement> {
    return this.httpClient.get<LeadMaintanceElement>(`api/v1/lead/`+id);
  }

  updateLeadById(id: Number, lead: LeadMaintanceElement): Observable<object> {
    return this.httpClient.put(`api/v1/lead/`+id, lead);
  }

  updateLeadmaintance(id: Number, lead: LeadMaintanceElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateLeadMaintance/`+id, lead);
  }

}
