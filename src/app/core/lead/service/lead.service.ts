import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LeadElement } from '../models/lead.model';
// import { Lead } from '../entities/lead';

@Injectable({
  providedIn: 'root',
})
export class LeadService {

  constructor(private httpClient: HttpClient) {}

  getLeadList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getLeads`);
  }

  createLead(lead:any): Observable<any> {
    const config ={
        "aadhar": lead.aadhar,
        "cityId": lead.city,
        "countryId": lead.country,
        "dateOfBirth": lead.dateOfBirth,
        "email": lead.email,
        "firstName": lead.firstName,
        "gender": lead.gender,
        // "id": lead.id,
        "lastName": lead.lastName,
       // "leadId": lead.leadId,
        "leadStage": lead.leadStage,
        "leadSourceId": lead.leadSource,
        "mobileNo": lead.mobileNo,
        "mobileNo2": lead.mobileNo2,
        "pan": lead.pan,
        "pincodeId": lead.postalCode,
        "stateId": lead.state,
        "userId": lead.userId,
        "createdBy":lead.createdBy,
        "area":lead.area,
        "leadStatusId":lead.leadStatus
    }
    console.log("lead",lead)
     return this.httpClient.post<any>(`api/v1/lead`, config);
  }

  getLeadById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/lead/`+id);
  }

  updateLeadById(id: Number, lead: any): Observable<object> {
    const config ={
      "aadhar": lead.aadhar,
      "cityId": lead.city,
      "countryId": lead.country,
      "dateOfBirth": lead.dateOfBirth,
      "email": lead.email,
      "firstName": lead.firstName,
      "gender": lead.gender,
      "id": lead.id,
      "lastName": lead.lastName,
      "leadId": lead.leadId,
      "leadStage": lead.leadStage,
      "leadSource": lead.leadSource,
      "mobileNo": lead.mobileNo,
      "mobileNo2": lead.mobileNo2,
      "pan": lead.pan,
      "pincodeId": lead.postalCode,
      "stateId": lead.state,
      "userId": lead.userId,
      "createdBy":lead.createdBy,
      "area":lead.area,
      "leadStatusId":lead.leadStatus
  }
    return this.httpClient.put(`api/v1/lead/`+id, config);
  }

  autoAssignUserByRole(id:any,role:any): Observable<any> {
    const config ={
        "userId": id,
        "userRole": role,
    }
    console.log("lead",config)
     return this.httpClient.post<any>(`api/v1/autoAssignUserByRole`, config);
  }
}
