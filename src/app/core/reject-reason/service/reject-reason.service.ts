import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RejectReasonElement } from '../models/reject-reason.model';

@Injectable({
  providedIn: 'root',
})
export class RejectReasonService {

  constructor(private httpClient: HttpClient) {}

  getRejectReasonList(): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getAllRegion`);
  }

  createRejectReason(rejectReason:any): Observable<any> {
    console.log("rejectReason",rejectReason)
     return this.httpClient.post<any>(`api/v1/createRegion`, rejectReason);
  }

  getRejectReasonById(id: Number): Observable<any> {
    return this.httpClient.get<any>(`api/v1/getRegionById/`+id);
  }

  updateRejectReasonById(id: Number, rejectReason: RejectReasonElement): Observable<object> {
    return this.httpClient.put(`api/v1/updateRegion/`+id, rejectReason);
  }

}
