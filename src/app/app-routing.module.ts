import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth/guards/auth.guard';
import { ReverseAuthGuard } from './core/auth/guards/reverse-auth.guard';
import { RoleGuardGuard } from './core/auth/guards/role-guard.guard';
import { PageError404Component } from './view/pages/page-error404/page-error404.component';

const routes: Routes = [
  {
    path:"",
    redirectTo:"home",
    pathMatch:'full'
  },
  {
    path: 'auth',
    loadChildren:()=> import("../app/view/pages/auth/auth.module").then(m=>m.AuthModule),
    canActivate: [ReverseAuthGuard]
  },
  {
    path: 'home',
    canActivate:[AuthGuard],
    children:[
      // { path: '', redirectTo: '', pathMatch: 'full' },
      { path: '',
        loadChildren:()=>import('./view/pages/dashboard/dashboard.module').then(m=>m.DashboardModule) },
      {
        path:'lead',
        canActivate:[RoleGuardGuard],
        data: { role: ['Sales','Call Center','Agency','Credit']},//Sales , CallCenter
        loadChildren:()=>import('./view/pages/lead/lead.module').then(m=>m.LeadModule)
      },
      {
        path:'enquiry',
        canActivate:[RoleGuardGuard],
        data: { role: ['CRM']},//Agency
        loadChildren:()=>import('./view/pages/enquiry/enquiry.module').then(m=>m.EnquiryModule)
      },
      {
        path:'enquiry-worklist',
        canActivate:[RoleGuardGuard],
        data: { role: ['Sales']},//Agency
        loadChildren:()=>import('./view/pages/enquiry-worklist/enquiry-worklist.module').then(m=>m.EnquiryWorklistModule)
      },
      {
        path:'manual-assignment',
        canActivate:[RoleGuardGuard],
        data: { role: ['Sales']},//Agency
        loadChildren:()=>import('./view/pages/manual-assignment/manual-assignment.module').then(m=>m.ManualAssignmentModule)
      },
      {
        path:'lead-maintance',
        canActivate:[RoleGuardGuard],
        data: { role: ['Agency']},//Agency
        loadChildren:()=>import('./view/pages/lead-maintance/lead-maintance.module').then(m=>m.LeadMaintanceModule)
      },
      {
        path:'worklist',
        canActivate:[RoleGuardGuard],
        data: { role: ['Credit','Sales']},//Credit , Sales
        loadChildren:()=>import('./view/pages/work-list/work-list.module').then(m=>m.WorkListModule)
      },
      {
        path:'user',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/user/user.module').then(m=>m.UserModule)
      },
      {
        path:'role',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/role/role.module').then(m=>m.RoleModule)
      },
      {
        path:'branch',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/branch/branch.module').then(m=>m.BranchModule)
      },
      {
        path:'user-department',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/user-department/user-department.module').then(m=>m.UserDepartmentModule)
      },
      {
        path:'user-designation',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/user-designation/user-designation.module').then(m=>m.UserDesignationModule)
      },
      {
        path:'lead-source',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/lead-source/lead-source.module').then(m=>m.LeadSourceModule)
      },
      {
        path:'lead-status',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/lead-status/lead-status.module').then(m=>m.LeadStatusModule)
      },
      {
        path:'reason-master',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/reason-master/reason-master.module').then(m=>m.ReasonMasterModule)
      },
      {
        path:'reject-reason',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/masters/reject-reason/reject-reason.module').then(m=>m.RejectReasonModule)
      },
      {
        path:'countries',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/geography-masters/countries/countries.module').then(m=>m.CountriesModule)
      },
      {
        path:'states',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/geography-masters/states/states.module').then(m=>m.StatesModule)
      },
      {
        path:'cities',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/geography-masters/cities/cities.module').then(m=>m.CitiesModule)
      },
      {
        path:'areas',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/geography-masters/areas/areas.module').then(m=>m.AreasModule)
      },
      {
        path:'pincode',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin']},
        loadChildren:()=>import('./view/pages/geography-masters/pincode/pincode.module').then(m=>m.PincodeModule)
      },
      {
        path:'user-profile',
        canActivate:[RoleGuardGuard],
        data: { role: ['Admin','Sales','Call Center','CRM','Agency','Credit']},
        loadChildren:()=>import('./view/pages/profile/profile.module').then(m=>m.ProfileModule)
      },
      // {
      //   path:'pages-error404',
      //   loadChildren:()=>import('./view/pages/page-error404/page-error404.module').then(m=>m.PageError404Module)
      // },
      { path: 'pages-error404', component: PageError404Component },
      { path: '**', redirectTo: 'pages-error404', pathMatch: 'full' },
    ]
  },
  { path: '**', redirectTo: 'auth', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
